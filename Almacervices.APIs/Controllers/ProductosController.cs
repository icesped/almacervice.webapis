﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Almacervice.Data.Repositories;
using Newtonsoft.Json;
using Almacervice.Model.Entities;
using AutoMapper;
using Almacervices.APIs.ViewModels;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using Almacervices.APIs.Core.Enum;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Almacervices.APIs.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ProductosController : Controller
    {
        private readonly ILogger _logger;
        private ProductoSucursalRepository _productoSucursalRepository;
        private ProductoRepository _productoRepository;
        private readonly JsonSerializerSettings _serializerSettings;

        //Contructor
        public ProductosController(ProductoSucursalRepository productoSucursalRepository, ProductoRepository productoRepository,
            ILoggerFactory loggerFactory)
        {
            _productoSucursalRepository = productoSucursalRepository;
            _productoRepository = productoRepository;
            _logger = loggerFactory.CreateLogger<ProductosController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        /// <summary>
        /// Obtiene un producto por su id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetProductById")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> GetProductById(int id)
        {
            try
            {
                var product = await _productoSucursalRepository.GetProductById(id);

                if (product == null)
                    return NotFound();

                var productDTO = Mapper.Map<ProductoSucursal, ProductoSucursalDTO>(product);
                var json = JsonConvert.SerializeObject(new { data = productDTO }, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetProductById: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Obtiene un producto base mediante su codigo de barra.
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        [HttpGet("{barcode}/bases")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> GetProductBase(long barcode)
        {
            if (barcode.ToString().Length >= 8)
            {
                try
                {
                    var producto = await _productoRepository.GetProductByBarcode(barcode);

                    if (producto != null)
                    {
                        var productoDTO = Mapper.Map<Producto, ProductoDTO>(producto);
                        var json = JsonConvert.SerializeObject(new { data = productoDTO }, _serializerSettings);

                        return new OkObjectResult(json);
                    }
                    else
                    {
                        _logger.LogInformation($"GetProductBase: El producto con codigo: {barcode}, no fue encontrado.");
                        return NotFound(new { message = "El producto base no existe." });
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetProductBase: Error al intentar obtener datos en la BD", ex.Message);
                    return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = "Error 503: Ocurrió un problema al intentar obtener la información, favor, contacte a soporte." });
                }
            }
            else
            {
                _logger.LogInformation($"GetProductBase: El producto con codigo: {barcode}, no corresponde a un producto base.");
                return NotFound(new { message = "El código no corresponde a un producto base." });
            }
        }

        /// <summary>
        /// Crea un nuevo producto
        /// </summary>
        /// <param name="productDTO"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> CreateProduct([FromBody] ProductoSucursalDTO productDTO)
        {
            try
            {
                // Valida el modelo
                if (!ModelState.IsValid)
                {
                    _logger.LogWarning($"CreateProduct: El objeto recibido no es valido");
                    return BadRequest("Los datos ingresados no son válidos, por favor revise e inténtelo de nuevo.");
                }

                // Valida la existencia del producto
                var productSubsidiary = await _productoSucursalRepository.GetProductByBarcodeAndSubsidiary(productDTO.Codigo, productDTO.SucursalId);

                if (productSubsidiary != null)
                {
                    _logger.LogWarning("CreateProduct: Producto ya existe en el sistema.");
                    return StatusCode((int)HttpStatusCode.Conflict, new { message = "El producto que intenta crear ya existe en el sistema." });
                }

                // Valida la existe del producto base, si no existe lo agrega a la base general.
                if (productDTO.Codigo.ToString().Length >= 8)
                {
                    var p = await _productoRepository.GetProductByBarcode(productDTO.Codigo);

                    if (p == null)
                    {
                        // Crea el producto base.
                        Producto productBase = new Producto();
                        productBase.Codigo = productDTO.Codigo;
                        productBase.Detalle = productDTO.Detalle;
                        productBase.Marca = productDTO.Marca;
                        productBase.Peso = productDTO.Peso;
                        productBase.UnidadMedidaId = productDTO.UnidadMedidaId;
                        productBase.FechaCreacion = DateTime.Now;
                        productBase.EstaVerificado = false;

                        _productoRepository.Add(productBase);
                        _productoRepository.Commit();

                        _logger.LogInformation($"CreateProduct: Se ha creado un nuevo producto base por el usuario: {productDTO.UsuarioModificador}");
                    }
                }

                // Crea el producto
                var product = new ProductoSucursal
                {
                    SucursalId = productDTO.SucursalId,
                    Codigo = productDTO.Codigo,
                    Detalle = productDTO.Detalle,
                    Marca = productDTO.Marca,
                    Peso = productDTO.Peso,
                    UnidadMedidaId = productDTO.UnidadMedidaId,
                    CategoriaId = productDTO.CategoriaId,
                    ProveedorId = productDTO.ProveedorId,
                    StockCritico = productDTO.StockCritico,
                    NivelStock = 0,
                    Costo = productDTO.Costo,
                    Margen = productDTO.Margen,
                    TipoMargenId = productDTO.TipoMargenId,
                    Precio = productDTO.Precio,
                    Cantidad = 1,
                    Total = 0,
                    Ganancia = (productDTO.Precio - productDTO.Costo),
                    FechaCreacion = DateTime.Now,
                    EstaEliminado = false,
                    FechaEliminacion = default(DateTime),
                    FechaModificacion = DateTime.Now,
                    UsuarioModificador = productDTO.UsuarioModificador
                };

                _productoSucursalRepository.Add(product);
                _productoSucursalRepository.Commit();

                // Arma la respuesta
                productDTO = Mapper.Map<ProductoSucursal, ProductoSucursalDTO>(product);

                var json = JsonConvert.SerializeObject(new { data = productDTO }, _serializerSettings);

                var response = CreatedAtRoute("GetProductById", new { controller = "Productos", Id = productDTO.Id }, json);

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"CreateProduct: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Actualiza un producto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="productoDTO"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> UpdateProduct(int id, [FromBody]ProductoSucursalDTO productoDTO)
        {
            try
            {
                // Valida el modelo de datos recibido
                if (!ModelState.IsValid)
                {
                    _logger.LogWarning($"Se han enviado datos que no corresponden a un producto desde la sucursal: { productoDTO.SucursalId }, productoId: { id }");
                    return BadRequest(new { message = "Los datos ingresados del producto no corresponden, favor revisar." });
                }

                // Valida que el producto existe.
                ProductoSucursal producto = await _productoSucursalRepository.GetProductById(id);

                if (producto == null)
                {
                    _logger.LogWarning($"Se intento actualizar un producto que no existe, sucursal: { productoDTO.SucursalId }, productoId: { id }");
                    return NotFound(new { message = "El producto no existe, le recomendamos crearlo." });
                }
                else
                {
                    // Actualiza.
                    producto.Detalle = productoDTO.Detalle;
                    producto.Marca = productoDTO.Marca;
                    producto.Peso = productoDTO.Peso;
                    producto.UnidadMedidaId = productoDTO.UnidadMedidaId;
                    producto.CategoriaId = productoDTO.CategoriaId;
                    producto.ProveedorId = productoDTO.ProveedorId;
                    producto.StockCritico = productoDTO.StockCritico;
                    producto.Costo = productoDTO.Costo;
                    producto.Margen = productoDTO.Margen;
                    producto.TipoMargenId = productoDTO.TipoMargenId;
                    producto.Ganancia = (productoDTO.Precio - productoDTO.Costo);
                    producto.Precio = productoDTO.Precio;
                    producto.FechaModificacion = DateTime.Now;
                    producto.UsuarioModificador = productoDTO.UsuarioModificador;

                    _productoSucursalRepository.Commit();
                    _logger.LogInformation($"Producto id: {id} actualizado correctamente");
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"UpdateProduct: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Elimina un producto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            try
            {
                var producto = await _productoSucursalRepository.GetProductById(id);

                if (producto == null)
                {
                    _logger.LogWarning($"Se intento eliminar un producto, pero no fue encontrado, productoId: { id }");
                    NotFound(new { message = "El producto que desea eliminar no existe." });
                }
                else
                {
                    producto.EstaEliminado = true;
                    producto.FechaEliminacion = DateTime.Now;
                    producto.FechaModificacion = DateTime.Now;
                    _productoRepository.Commit();
                    _logger.LogInformation($"Producto {producto} eliminado.");
                }
               
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"DeleteProduct: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }
    }
}
