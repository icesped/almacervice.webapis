﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Almacervice.Data.Repositories;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Almacervice.Model.Entities;
using System.Net;
using Almacervices.APIs.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Almacervices.APIs.Core.Utils;
using Almacervices.APIs.Core.Enum;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Almacervices.APIs.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ProveedoresController : Controller
    {
        private readonly ILogger _logger;
        private ProveedorRepository _proveedorRepository;
        private readonly JsonSerializerSettings _serializerSettings;

        //Contructor
        public ProveedoresController(ProveedorRepository proveedorRepository, ILoggerFactory loggerFactory)
        {
            _proveedorRepository = proveedorRepository;
            _logger = loggerFactory.CreateLogger<ProveedoresController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpPost]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> CreateNewProvider([FromBody]ProveedorDTO proveedorDTO)
        {
            try
            {
                // Valida el modelo
                if (!ModelState.IsValid)
                {
                    _logger.LogWarning($"CreateNewProvider: El objeto recibido no es valido");
                    return BadRequest("Los datos ingresados no son válidos, por favor revise e inténtelo de nuevo.");
                }
                // Valida el rut recibido
                #region Valida Rut
                try
                {
                    if (string.IsNullOrWhiteSpace(proveedorDTO.Rut.ToString()))
                    {
                        _logger.LogWarning($"CreateNewProvider-GetRutDvValidated: El rut no es valido");
                        return BadRequest(new { message = "El rut ingresado no es válido, inténtelo nuevamente." });
                    }
                    else
                    {
                        string[] rut = RutUtil.GetRutDvValidated(string.Concat(proveedorDTO.Rut.ToString(), proveedorDTO.Dv.ToString()));

                        if (rut == null)
                        {
                            _logger.LogWarning($"CreateNewProvider-GetRutDvValidated: El rut no es valido");
                            return BadRequest(new { message = "El rut ingresado no es válido, inténtelo nuevamente." });
                        }

                        proveedorDTO.Rut = Convert.ToInt32(rut[0]);
                        proveedorDTO.Dv = rut[1].ToCharArray()[0];
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError((int)ErrorCodeEnum.InvalidRutError, ex, $"CreateStore-GetRutDvValidated: Error al intentar validar el rut");
                    return BadRequest(new { message = $"Error 400: Ocurrió un problema al intentar validar el RUT, favor, contacte a soporte." });
                }
                #endregion

                // Valida la existencia del proveedor
                Proveedor proveedor;
                proveedor = await _proveedorRepository.GetProviderBySubsidiaryId(proveedorDTO.Rut, proveedorDTO.SucursalId);

                if (proveedor != null)
                {
                    _logger.LogWarning("CreateNewProvider: Proveedor ya existe en el sistema.");
                    return StatusCode((int)HttpStatusCode.Conflict, new { message = "El proveedor que intenta crear ya existe en el sistema." });
                }


                proveedor = new Proveedor
                {
                    SucursalId = proveedorDTO.SucursalId,
                    Rut = proveedorDTO.Rut,
                    Dv = proveedorDTO.Dv,
                    Nombre = proveedorDTO.Nombre,
                    NombreContacto = proveedorDTO.NombreContacto,
                    CargoContacto = proveedorDTO.CargoContacto,
                    TelefonoContacto = proveedorDTO.TelefonoContacto,
                    CelularContacto = proveedorDTO.CelularContacto,
                    Email = proveedorDTO.Email,
                    SitioWeb = proveedorDTO.SitioWeb,
                    Direccion = proveedorDTO.Direccion,
                    PaisId = proveedorDTO.PaisId,
                    RegionId = proveedorDTO.RegionId,
                    ProvinciaId = proveedorDTO.ProvinciaId,
                    ComunaId = proveedorDTO.ComunaId,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    UsuarioModificador = proveedorDTO.UsuarioModificador,
                    EstaEliminado = false,
                    FechaEliminacion = default(DateTime)
                };

                _proveedorRepository.Add(proveedor);
                _proveedorRepository.Commit();

                // Mapea el proveedor
                proveedorDTO = Mapper.Map<Proveedor, ProveedorDTO>(proveedor);

                // Serializa y responde con un objeto Usuario
                CreatedAtRouteResult response =
                    CreatedAtRoute(
                        "GetStoreById",
                        new { controller = "Proveedor", id = proveedorDTO.Id }, new
                        {
                            negocio = new
                            {
                                proveedorDTO
                            } 
                        });

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateNewProvider: Error al intentar conectar con la BD", ex.Message);
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error {(int)ErrorCodeEnum.BDUnavailableError}: Ocurrió un problema al intentar crear su producto, favor, contacte a soporte." });
            }

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
