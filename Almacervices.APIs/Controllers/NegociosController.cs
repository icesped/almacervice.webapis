﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Almacervice.Model.Entities;
using Almacervices.APIs.ViewModels;
using AutoMapper;
using Almacervice.Data.Abstract;
using Microsoft.AspNetCore.Authorization;
using Almacervices.APIs.Core.Utils;
using Almacervice.Data.Repositories;
using System.Net;
using Almacervices.APIs.Core.Enum;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Almacervices.APIs.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class NegociosController : Controller
    {
        private readonly ILogger _logger;
        private NegocioRepository _negocioRepository;
        private SucursalRepository _sucursalRepository;
        private ProveedorRepository _proveedorRepository;
        private readonly JsonSerializerSettings _serializerSettings;

        //Contructor
        public NegociosController(
            NegocioRepository negocioRepository,
            SucursalRepository sucursalRepository,
            ProveedorRepository proveedorRepository,
            ILoggerFactory loggerFactory)
        {
            _negocioRepository = negocioRepository;
            _sucursalRepository = sucursalRepository;
            _proveedorRepository = proveedorRepository;
            _logger = loggerFactory.CreateLogger<NegociosController>();
            _serializerSettings = new JsonSerializerSettings {Formatting = Formatting.Indented};
        }

        /// <summary>
        /// Obtiene todos los negocios
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "Almacervice")]
        public IActionResult GetAllStores()
        {
            try
            {
                IEnumerable<Negocio> stores = _negocioRepository.GetAllStores();
                IEnumerable<NegocioDTO> storesDTO = Mapper.Map<IEnumerable<Negocio>, IEnumerable<NegocioDTO>>(stores);
                var json = JsonConvert.SerializeObject(new { stores = storesDTO }, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetAllStores: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Obtiene un negocio por su id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetStoreById")]
        [Authorize(Policy = "Almacervice")]
        public IActionResult GetStoreById(int id)
        {
            try
            {
                var store = _negocioRepository.GetStoreById(id);

                if (store == null)
                    return NotFound();

                var storeDTO = Mapper.Map<Negocio, NegocioDTO>(store);
                var json = JsonConvert.SerializeObject(storeDTO, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetStoreById: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Crea un nuevo negocio
        /// </summary>
        /// <param name="storeDTO"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Policy = "Almacervice")]
        public IActionResult CreateStore([FromBody]NegocioDTO storeDTO)
        {
            try
            {
                // Valida modelo
                if (!ModelState.IsValid)
                {
                    _logger.LogWarning($"CreateStore: Objeto recibido no es válido");
                    return BadRequest(new { message = "Los datos ingresados no son válidos, por favor revise e inténtelo de nuevo." });
                }

                // Valida el rut recibido
                if (string.IsNullOrWhiteSpace(storeDTO.Rut.ToString()))
                {
                    _logger.LogWarning($"CreateStore-GetRutDvValidated: El rut no es valido");
                    return BadRequest(new { message = "El rut ingresado no es válido, inténtelo nuevamente." });
                }
                else
                {
                    string[] rut = RutUtil.GetRutDvValidated(string.Concat(storeDTO.Rut.ToString(), storeDTO.Dv.ToString()));

                    if (rut == null)
                    {
                        _logger.LogWarning($"CreateStore-GetRutDvValidated: El rut no es valido");
                        return BadRequest(new { message = "El rut ingresado no es válido, inténtelo nuevamente." });
                    }

                    storeDTO.Rut = Convert.ToInt32(rut[0]);
                    storeDTO.Dv = rut[1].ToCharArray()[0];
                }

                // Valida si existe el negocio
                var storeExist = _negocioRepository.GetStoreByRut(storeDTO.Rut);
                if (storeExist != null)
                {
                    _logger.LogWarning("CreateStore-GetStoreByRut: El rut ingresado ya existe en el sistema");
                    return StatusCode((int)HttpStatusCode.Conflict, new { message = "El rut ingresado ya existe en el sistema" });
                }

                // Guarda el nuevo negocio
                var store = new Negocio
                {
                    Rut = storeDTO.Rut,
                    Dv = storeDTO.Dv,
                    RazonSocial = storeDTO.RazonSocial,
                    DireccionFacturacion = storeDTO.DireccionFacturacion,
                    ComunaFacturacionId = storeDTO.ComunaFacturacionId,
                    ProvinciaFacturacionId = storeDTO.ProvinciaFacturacionId,
                    RegionFacturacionId = storeDTO.RegionFacturacionId,
                    PaisId = storeDTO.PaisId,
                    GiroComercial = storeDTO.GiroComercial,
                    Telefono = storeDTO.Telefono,
                    Email = storeDTO.GiroComercial,
                    NombreContactoUno = storeDTO.NombreContactoUno,
                    TelefonoContactoUno = storeDTO.TelefonoContactoUno,
                    NombreContactoDos = storeDTO.NombreContactoDos,
                    TelefonoContactoDos = storeDTO.TelefonoContactoDos,
                    UsaDTE = false,
                    PermiteCreditoClientes = false,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    EstaSuspendido = false,
                    FechaSuspension = default(DateTime),
                    EstaEliminado = false,
                    FechaEliminacion = default(DateTime)

                };
                _negocioRepository.Add(store);
                _negocioRepository.Commit();

                // Mapea el nuevo negocio
                storeDTO = Mapper.Map<Negocio, NegocioDTO>(store);

                // Crea la sucursal matriz
                var subsidiary = new Sucursal
                {
                    NegocioId = storeDTO.Id,
                    EsMatriz = true,
                    Telefono = storeDTO.Telefono,
                    Direccion = storeDTO.DireccionFacturacion,
                    ComunaId = storeDTO.ComunaFacturacionId,
                    ProvinciaId = storeDTO.ProvinciaFacturacionId,
                    RegionId = storeDTO.RegionFacturacionId,
                    PaisId = storeDTO.PaisId,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    EstaEliminado = false,
                    FechaEliminacion = default(DateTime)
                };
                _sucursalRepository.Add(subsidiary);
                _sucursalRepository.Commit();

                // Mapea la sucursal
                var subsidiaryDTO = Mapper.Map<Sucursal, SucursalDTO>(subsidiary);

                // Crea un proveedor por defecto
                Proveedor provider = new Proveedor
                {
                    SucursalId = subsidiaryDTO.Id,
                    Rut = 99999999,
                    Dv = '1',
                    Nombre = "Sin Proveedor",
                    ComunaId = subsidiaryDTO.ComunaId,
                    ProvinciaId = subsidiaryDTO.ProvinciaId,
                    RegionId = subsidiaryDTO.RegionId,
                    PaisId = subsidiaryDTO.PaisId,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    UsuarioModificador = "ALMACERVICE",
                    EstaEliminado = false
                };
                _proveedorRepository.Add(provider);
                _proveedorRepository.Commit();

                // Serializa y responde con un objeto Usuario
                CreatedAtRouteResult response =
                    CreatedAtRoute(
                        "GetStoreById",
                        new { controller = "Negocios", id = storeDTO.Id }, new
                        {
                            negocio = new
                            {
                                storeDTO
                            },
                            sucursal = new
                            {
                                subsidiaryDTO
                            }
                        });

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"CreateStore: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Actualiza un negocio
        /// </summary>
        /// <param name="id"></param>
        /// <param name="storeDTO"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult UpdateStore(int id, [FromBody]NegocioDTO storeDTO)
        {
            try
            {
                // Valida modelo
                if (!ModelState.IsValid)
                {
                    _logger.LogWarning($"UpdateStore: Objeto recibido no es válido");
                    return BadRequest(new { message = "Los datos ingresados no son válidos, por favor revise e inténtelo de nuevo." });
                }

                var store = _negocioRepository.GetStoreById(id);

                if (store == null)
                    return NotFound();
                else
                {
                    store.DireccionFacturacion = storeDTO.DireccionFacturacion;
                    store.ComunaFacturacionId = storeDTO.ComunaFacturacionId;
                    store.ProvinciaFacturacionId = storeDTO.ProvinciaFacturacionId;
                    store.RegionFacturacionId = storeDTO.RegionFacturacionId;
                    store.PaisId = storeDTO.RegionFacturacionId;
                    store.GiroComercial = storeDTO.GiroComercial;
                    store.Email = storeDTO.Email;
                    store.NombreContactoUno = storeDTO.NombreContactoUno;
                    store.TelefonoContactoUno = storeDTO.TelefonoContactoUno;
                    store.NombreContactoDos = storeDTO.NombreContactoDos;
                    store.TelefonoContactoDos = storeDTO.TelefonoContactoDos;
                    store.FechaModificacion = DateTime.Now;
                    _negocioRepository.Commit();
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"UpdateStore: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var store = _negocioRepository.GetStoreById(id);

            if (store == null)
                return new NotFoundResult();

            _negocioRepository.Delete(store);
            _negocioRepository.Commit();

            return NoContent();
        }

        #region SUCURSALES POR NEGOCIO
        /// <summary>
        /// Obtiene todas las sucursales de un negocio
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}/sucursales")]
        public async Task<IActionResult> GetAllSubsidiariesByStore(int id)
        {
            try
            {
                var sucursales = await _sucursalRepository.GetAllSubsidiariesByStoreId(id);
                IEnumerable<SucursalDTO> sucursalesDTO = Mapper.Map<IEnumerable<Sucursal>, IEnumerable<SucursalDTO>>(sucursales);
                var json = JsonConvert.SerializeObject(new { sucursales = sucursalesDTO }, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetAllSubsidiariesByStore: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        } 
        #endregion

    }
}
