﻿using Almacervice.Data.Abstract;
using Almacervice.Data.Repositories;
using Almacervices.APIs.Core.Enum;
using Almacervices.APIs.Core.Exceptions;
using Almacervices.APIs.Core.JwtOptions;
using Almacervices.APIs.Core.Utils;
using Almacervices.APIs.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Almacervices.APIs.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AuthController : Controller
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ILogger _logger;
        private readonly JsonSerializerSettings _serializerSettings;
        private UsuarioRepository _usuarioRepository;

        // Constructor
        public AuthController(IOptions<JwtIssuerOptions> jwtOptions, ILoggerFactory loggerFactory, UsuarioRepository usuarioRepository)
        {
            _jwtOptions = jwtOptions.Value;
            ThrowIfInvalidOptions(_jwtOptions);
            _usuarioRepository = usuarioRepository;

            _logger = _logger = loggerFactory.CreateLogger<AuthController>();
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
        }

        /// <summary>
        /// Valida las credenciales para iniciar sesión en el sistema.
        /// </summary>
        /// <param name="usuarioDTO"></param>
        /// <returns>Token de Usuario</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]UsuarioDTO usuarioDTO)
        {
            try
            {
                //TODO: Validar que el mail este confirmado al iniciar sesion.
                //TODO: Validar si la cuenta ya esta logueada.
                //TODO: Comprobar que la cuenta no este suspendida.
                // Valida el modelo
                if (!ModelState.IsValid)
                {
                    _logger.LogWarning($"Login: Objeto recibido no es válido");
                    return BadRequest(new { message = "Los datos ingresados no son válidos, por favor revise e inténtelo de nuevo." });
                }
                // Valida el rut recibido
                if (string.IsNullOrWhiteSpace(usuarioDTO.Rut.ToString()))
                {
                    _logger.LogWarning($"Login-GetRutDvValidated: El rut no es valido");
                    return BadRequest(new { message = "El rut ingresado no es válido, inténtelo nuevamente." });
                }
                else
                {
                    string[] rut = RutUtil.GetRutDvValidated(string.Concat(usuarioDTO.Rut.ToString(), usuarioDTO.Dv.ToString()));

                    if (rut == null)
                    {
                        _logger.LogWarning($"Login-GetRutDvValidated: El rut no es valido");
                        return BadRequest(new { message = "El rut ingresado no es válido, inténtelo nuevamente." });
                    }

                    usuarioDTO.Rut = Convert.ToInt32(rut[0]);
                    usuarioDTO.Dv = rut[1].ToCharArray()[0];
                }


                // Obtiene la identidad del usuario con credenciales validas
                var identity = await GetClaimsIdentity(usuarioDTO);

                if (identity == null)
                {
                    _logger.LogInformation($"Login: No fue posible obtener Claims, Rut ({usuarioDTO.Rut}) o clave no válidos");
                    return BadRequest(new { message = "El Rut o la Contraseña no son válidos. Es posible que no existe la cuenta." });
                }

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, usuarioDTO.Rut.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                    new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                    identity.FindFirst("AlmacerviceUser")
                };

                // Crea el JWT y lo encripta.
                var jwt = new JwtSecurityToken(
                    issuer: _jwtOptions.Issuer,
                    audience: _jwtOptions.Audience,
                    claims: claims,
                    notBefore: _jwtOptions.NotBefore,
                    expires: _jwtOptions.Expiration,
                    signingCredentials: _jwtOptions.SigningCredentials);

                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                // Serializa y retorna la respuesta
                var response = new
                {
                    data = new
                    {
                        access_token = encodedJwt,
                        expires_in = (int)_jwtOptions.ValidFor.TotalSeconds
                    }
                };

                var json = JsonConvert.SerializeObject(response, _serializerSettings);
                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"Login: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Obtiene la reclamación de identidad del usuario
        /// </summary>
        /// <param name="usuarioDTO"></param>
        /// <returns></returns>
        private Task<ClaimsIdentity> GetClaimsIdentity(UsuarioDTO usuarioDTO)
        {
            if (AreValidCredentialsAndActiveAccount(usuarioDTO.Rut, usuarioDTO.Password))
            {
                return Task.FromResult(new ClaimsIdentity(
                    new GenericIdentity(usuarioDTO.Rut.ToString(), "Token"),
                    new[]
                    {
                        new Claim("AlmacerviceUser", "UsuarioAuthorizado")
                    })
                );
            }

            // Retorno si las credenciales no son válidas o la cuenta no existe.
            return Task.FromResult<ClaimsIdentity>(null);
        }

        /// <summary>
        /// Valida que el rut exista y la contraseña concuerde.
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private bool AreValidCredentialsAndActiveAccount(int rut, string password)
        {
            // Obtiene los datos del usuario (salt y clave hash)
            var usuario = _usuarioRepository.GetUserByRut(rut);

            if (usuario != null && PasswordUtil.ValidatePassword(password, usuario.Password, usuario.Salt))
                return true;

            return false;
        }

        /// <summary>
        /// Tiempo convertido en segundos
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private static long ToUnixEpochDate(DateTime date)
          => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);

        /// <summary>
        /// Valida las opciones del JWT 
        /// </summary>
        /// <param name="options"></param>
        private static void ThrowIfInvalidOptions(JwtIssuerOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (options.ValidFor <= TimeSpan.Zero)
                throw new ArgumentException("Debe ser un TimeSpan distinto de cero", nameof(JwtIssuerOptions.ValidFor));

            if (options.SigningCredentials == null)
                throw new ArgumentNullException(nameof(JwtIssuerOptions.SigningCredentials));

            if (options.JtiGenerator == null)
                throw new ArgumentNullException(nameof(JwtIssuerOptions.JtiGenerator));
        }
    }
}
