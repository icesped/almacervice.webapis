﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Almacervice.Data.Repositories;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using AutoMapper;
using Almacervice.Model.Entities;
using Almacervices.APIs.ViewModels;
using Almacervices.APIs.Core.Enum;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Almacervices.APIs.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CategoriasController : Controller
    {
        private readonly ILogger _logger;
        private CategoriaRepository _categoriaRepository;
        private readonly JsonSerializerSettings _serializerSettings;

        //Contructor
        public CategoriasController(CategoriaRepository categoriaRepository, ILoggerFactory loggerFactory)
        {
            _categoriaRepository = categoriaRepository;
            _logger = loggerFactory.CreateLogger<CategoriasController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        /// <summary>
        /// Obtiene todas las categorias
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> GetAllCategories()
        {
            try
            {
                var categorias = await _categoriaRepository.GetAllCategories();
                IEnumerable<CategoriaDTO> categoriasDTO = Mapper.Map<IEnumerable<Categoria>, IEnumerable<CategoriaDTO>>(categorias);
                var json = JsonConvert.SerializeObject(new { data = categoriasDTO }, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetAllProvidersBySubsidiaryId: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
