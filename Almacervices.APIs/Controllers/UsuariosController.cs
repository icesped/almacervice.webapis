﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Almacervice.Model.Entities;
using Almacervices.APIs.ViewModels;
using AutoMapper;
using Almacervices.APIs.Core.Utils;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Almacervice.Data.Repositories;
using System.Net;
using Almacervices.APIs.Core.Enum;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Almacervices.APIs.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UsuariosController : Controller
    {
        private readonly ILogger _logger;
        private UsuarioRepository _usuarioRepository;
        private readonly JsonSerializerSettings _serializerSettings;

        //Contructor
        public UsuariosController(UsuarioRepository usuarioRepository, ILoggerFactory loggerFactory)
        {
            _usuarioRepository = usuarioRepository;
            _logger = loggerFactory.CreateLogger<UsuariosController>();
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
        }

        /// <summary>
        /// Obtiene todos los usuarios del sistema
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                IEnumerable<Usuario> users = await _usuarioRepository.GetAllUsers();
                IEnumerable<UsuarioDTO> usersDTO = Mapper.Map<IEnumerable<Usuario>, IEnumerable<UsuarioDTO>>(users);
                var json = JsonConvert.SerializeObject(new { users = usersDTO }, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetAllUsers: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Obtiene un usuario por medio de su id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetUserById")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> GetUserById(int id)
        {
            try
            {
                var user = await _usuarioRepository.GetUserById(id);

                if (user == null)
                {
                    _logger.LogInformation($"GetUserById: El usuario: {id} no fue encontrado.");
                    return NotFound(new { message = "El usuario no existe." });
                }

                var userDTO = Mapper.Map<Usuario, UsuarioDTO>(user);
                var json = JsonConvert.SerializeObject(userDTO, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetUserById: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Crea un nuevo usuario
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public IActionResult CreateUser([FromBody]UsuarioDTO userDTO)
        {
            try
            {
                // Valida que el Rut y la Clave no sean null y que el modelo sea valido.
                if (!ModelState.IsValid)
                {
                    _logger.LogWarning($"CreateUser: El objeto recibido no es valido");
                    return BadRequest(new { message = "Los datos ingresados no son válidos, por favor revise e inténtelo de nuevo." });
                }

                // Validación de RUT
                if (string.IsNullOrWhiteSpace(userDTO.Rut.ToString()))
                {
                    _logger.LogWarning($"CreateUser-GetRutDvValidated: El rut no es valido");
                    return BadRequest(new { message = "El rut ingresado no es válido, inténtelo nuevamente." });
                }
                else
                {
                    string[] rut = RutUtil.GetRutDvValidated(string.Concat(userDTO.Rut.ToString(), userDTO.Dv.ToString()));

                    if (rut == null)
                    {
                        _logger.LogWarning($"CreateUser-GetRutDvValidated: El rut no es valido");
                        return BadRequest(new { message = "El rut ingresado no es válido, inténtelo nuevamente." });
                    }

                    userDTO.Rut = Convert.ToInt32(rut[0]);
                    userDTO.Dv = rut[1].ToCharArray()[0];
                }

                // Validación usuario existente
                var userExist = _usuarioRepository.GetUserByRut(userDTO.Rut);
                if (userExist != null)
                {
                    _logger.LogWarning("CreateUser-GetUserByRut: Ya existe el usuario que se intenta crear.");
                    return StatusCode((int)HttpStatusCode.Conflict, new { message = "El rut ingresado ya tiene una cuenta en activa en el sistema" });
                }


                // Encriptación de contraseña
                var salt = PasswordUtil.GetSalt();
                var passHashed = PasswordUtil.GetHashWithSalt(userDTO.Password, salt);

                var user = new Usuario
                {
                    Rut = userDTO.Rut,
                    Dv = userDTO.Dv,
                    Nombre = userDTO.Nombre,
                    ApellidoPaterno = userDTO.ApellidoPaterno,
                    ApellidoMaterno = userDTO.ApellidoMaterno,
                    Email = userDTO.Email,
                    EmailConfirmado = false,
                    Password = passHashed,
                    Salt = salt,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    EstaSuspendido = false,
                    FechaSuspension = default(DateTime),
                    EstaEliminado = false,
                    FechaEliminacion = default(DateTime)
                };

                _usuarioRepository.Add(user);
                _usuarioRepository.Commit();

                userDTO = Mapper.Map<Usuario, UsuarioDTO>(user);

                CreatedAtRouteResult response =
                    CreatedAtRoute(
                        "GetUserById",
                        new { controller = "Usuarios", id = userDTO.Id }, new
                        {
                            usuario = new
                            {
                                userDTO.Id,
                                userDTO.Nombre,
                                userDTO.ApellidoPaterno,
                                userDTO.ApellidoMaterno,
                                userDTO.Email,
                                userDTO.FechaUltimoLogin
                            }
                        });

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"CreateUser: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Actualiza un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> UpdateUser(int id, [FromBody]UsuarioDTO userDTO)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    _logger.LogWarning($"UpdateUser: El objeto recibido no es valido");
                    return BadRequest("Los datos ingresados no son válidos, por favor revise e inténtelo de nuevo.");
                }

                Usuario usuario = await _usuarioRepository.GetUserById(id);

                if (usuario == null)
                {
                    _logger.LogInformation($"UpdateUser: El usuario no existe.");
                    return NotFound(new { message = "El usuario que intenta actualizar no existe, le recomendamos crearlo." });
                }
                else
                {
                    usuario.Nombre = userDTO.Nombre;
                    usuario.ApellidoPaterno = userDTO.ApellidoPaterno;
                    usuario.ApellidoMaterno = userDTO.ApellidoMaterno;
                    usuario.FechaModificacion = DateTime.Now;
                    _usuarioRepository.Commit();
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"CreateUser: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Elimina un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var user = await _usuarioRepository.GetUserById(id);

                if (user == null)
                {
                    _logger.LogInformation($"DeleteUser: El usuario no existe.");
                    return NotFound(new { message = "El usuario que intenta eliminar no existe." });
                }

                _usuarioRepository.Delete(user);
                _usuarioRepository.Commit();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"DeleteUser: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }
    }
}
