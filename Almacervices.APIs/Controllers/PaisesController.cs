﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Almacervice.Model.Entities;
using Almacervices.APIs.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Almacervice.Data.Repositories;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Almacervices.APIs.Controllers
{
    [Route("api/[controller]")]
    public class PaisesController : Controller
    {
        private PaisRepository _paisRepository;
        private readonly JsonSerializerSettings _serializerSettings;
        
        //Constructor
        public PaisesController(PaisRepository paisRepository)
        {
            _paisRepository = paisRepository;
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        /// <summary>
        /// Obtiene todos los paises.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "Almacervice")]
        public IActionResult GetAllCountries()
        {
            IEnumerable<Pais> paises = _paisRepository.GetAllCountries();
            IEnumerable<PaisDTO> paisesDTO = Mapper.Map<IEnumerable<Pais>, IEnumerable<PaisDTO>>(paises);
            var json = JsonConvert.SerializeObject(paisesDTO, _serializerSettings);

            return new OkObjectResult(json);
        }
    }
}
