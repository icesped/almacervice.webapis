﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Almacervice.Data.Repositories;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Almacervice.Model.Entities;
using System.Net;
using AutoMapper;
using Almacervices.APIs.ViewModels;
using Almacervices.APIs.Core.Enum;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Almacervices.APIs.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class SucursalesController : Controller
    {
        private readonly ILogger _logger;
        private SucursalRepository _sucursalRepository;
        private ProductoSucursalRepository _productoSucursalRepository;
        private ProveedorRepository _proveedorRepository;
        private readonly JsonSerializerSettings _serializerSettings;

        // Constructor
        public SucursalesController(
            SucursalRepository sucursalRepository,
            ProductoSucursalRepository productoSucursalRepository,
            ProveedorRepository proveedorRepository,
            ILoggerFactory loggerFactory)
        {
            _sucursalRepository = sucursalRepository;
            _productoSucursalRepository = productoSucursalRepository;
            _proveedorRepository = proveedorRepository;
            _logger = loggerFactory.CreateLogger<SucursalesController>();
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
        }

        #region PRODUCTOS POR SUCURSAL
        /// <summary>
        /// Obtiene un producto por su codigo de barra.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="barcode"></param>
        /// <returns></returns>
        [HttpGet("{id}/productos/{barcode}", Name = "GetProductByBarcode")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> GetProductByBarcode(int id, long barcode)
        {
            try
            {
                var producto = await _productoSucursalRepository.GetProductByBarcodeAndSubsidiary(barcode, id);

                if (producto == null)
                {
                    _logger.LogInformation($"GetProductByBarcode: El producto con codigo: {barcode}, de la sucursal {id}, no fue encontrado.");
                    return NotFound(new { message = "El producto no existe, le recomendamos crearlo." });
                }

                var productoDTO = Mapper.Map<ProductoSucursal, ProductoSucursalDTO>(producto);
                var json = JsonConvert.SerializeObject(productoDTO, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetProductByBarcode: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Obtiene todos los productos de una sucursal
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}/productos")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> GetAllProductsBySubsidiaryId(int id)
        {
            try
            {
                var productos = await _productoSucursalRepository.GetAllProductsBySubsidiaryId(id);

                IEnumerable<ProductoSucursalDTO> productosDTO = Mapper.Map<IEnumerable<ProductoSucursal>, IEnumerable<ProductoSucursalDTO>>(productos);
                var json = JsonConvert.SerializeObject(new { data = productosDTO }, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetAllProductsBySubsidiaryId: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }
        #endregion

        #region PROVEEDORES POR SUCURSAL
        /// <summary>
        /// Obtiene todas los proveedores asociados a una sucursal
        /// </summary>
        /// <param name="sucursalId"></param>
        /// <returns></returns>
        [HttpGet("{id}/proveedores")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> GetAllProvidersBySubsidiaryId(int id)
        {
            try
            {
                var proveedores = await _proveedorRepository.GetAllProvidersBySubsidiaryId(id);
                IEnumerable<ProveedorDTO> proveedoresDTO = Mapper.Map<IEnumerable<Proveedor>, IEnumerable<ProveedorDTO>>(proveedores);
                var json = JsonConvert.SerializeObject(new { data = proveedoresDTO }, _serializerSettings);

                return new OkObjectResult(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetAllProvidersBySubsidiaryId: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        /// <summary>
        /// Obtiene un proveedor de acuerdo a la sucursal y su id
        /// </summary>
        /// <param name="sucursalId"></param>
        /// <returns></returns>
        [HttpGet("{id}/proveedores/{providerId}")]
        [Authorize(Policy = "Almacervice")]
        public async Task<IActionResult> GetProviderBySubsidiaryId(int providerId, int id)
        {
            try
            {
                var proveedor = await _proveedorRepository.GetProviderBySubsidiaryId(providerId, id);

                if (proveedor == null)
                {
                    _logger.LogInformation($"GetProviderBySubsidiaryId: El proveedor {providerId}, de la sucursal {id}, no fue encontrado.");
                    return NotFound(new { message = "El proveedor no existe, le recomendamos crearlo." });
                }

                var proveedorDTO = Mapper.Map<Proveedor, ProveedorDTO>(proveedor);
                var json = JsonConvert.SerializeObject(proveedorDTO, _serializerSettings);

                return new OkObjectResult(json);
            }

            catch (Exception ex)
            {
                _logger.LogError((int)ErrorCodeEnum.BDUnavailableError, ex, $"GetProviderBySubsidiaryId: Error al intentar conectar con la BD");
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, new { message = $"Error { (int)ErrorCodeEnum.BDUnavailableError }: Ocurrió un problema al intentar conectar con la Base de Datos, favor, contacte a soporte." });
            }
        }

        #endregion



        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
