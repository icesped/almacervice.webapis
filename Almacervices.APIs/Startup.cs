﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Almacervice.Data;
using Almacervice.Data.Abstract;
using Almacervice.Data.Repositories;
using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Serialization;
using MySQL.Data.Entity.Extensions;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Almacervices.APIs.ViewModels.Mappings;
using Almacervices.APIs.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Almacervices.APIs.Core.JwtOptions;
using System;
using NLog.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Almacervice.APIs
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        private readonly ILogger _logger;
        private readonly SymmetricSecurityKey _signingKey;

        public Startup(IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            // Obtiene la clave secreta para la generacion de JWT
            string SecretKey = Configuration["SecretKey:Key"];
            _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));

            _logger = loggerFactory.CreateLogger<Startup>();
        }

        /// <summary>
        /// Add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // TODO: Encriptar el string de conexion
            services.AddDbContext<AlmacerviceContext>(options =>
                options.UseMySQL(Configuration["AlmacerviceConnection:ConnectionString"],
                b => b.MigrationsAssembly("Almacervices.APIs"))
                .ConfigureWarnings(warnings => warnings.Throw(RelationalEventId.QueryClientEvaluationWarning)));

            // Repositories
            services.AddScoped<PaisRepository, PaisRepository>();
            services.AddScoped<UsuarioRepository, UsuarioRepository>();
            services.AddScoped<ProductoRepository, ProductoRepository>();
            services.AddScoped<ProductoSucursalRepository, ProductoSucursalRepository>();
            services.AddScoped<NegocioRepository, NegocioRepository>();
            services.AddScoped<SucursalRepository, SucursalRepository>();
            services.AddScoped<ProveedorRepository, ProveedorRepository>();
            services.AddScoped<CategoriaRepository, CategoriaRepository>();
            services.AddScoped<UnidadMedidaRepository, UnidadMedidaRepository>();
            services.AddScoped<TipoMargenRepository, TipoMargenRepository>();

            AutoMappingConfiguration.Configure();

            // Habilita Cors
            services.AddCors();

            // Establece la obligatoriedad de la autenticación. (es decir, bloqueado)
            // Bloquea todo excepto los que esten abiertos explicitamente).
            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                                    .RequireAuthenticatedUser()
                                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            })
            .AddJsonOptions(opts =>
            {
                // Fuerza el camel case a JSON
                opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            services.AddApiVersioning();

            // Obtiene opciones del appsettings.json
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            // Configurando JwtIssuerOptions
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

            // Usa politicas de autenticación
            services.AddAuthorization(options =>
            {
                options.AddPolicy("Almacervice",
                                  policy => policy.RequireClaim("AlmacerviceUser", "UsuarioAuthorizado"));
            });
        }

        /// <summary>
        /// Configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddNLog();
            

            app.UseStaticFiles();

            AlmacerviceDbInitializer.Initialize(app.ApplicationServices);

            // Add MVC to the request pipeline.
            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                );

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            // Habilita validación de Token configurando los parámetros
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = true,
                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };

            // Habilita el Middleware que se encarga de Autenticar y Autorizar.
            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });


            app.UseExceptionHandler(
              builder =>
              {
                  builder.Run(
                    async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if (error != null)
                        {
                            _logger.LogError(error.ToString());
                            context.Response.AddApplicationError(error.Error.Message);
                            await context.Response.WriteAsync(error.Error.Message).ConfigureAwait(false);
                        }
                    });
              });

            app.UseMvc();
        }
    }
}
