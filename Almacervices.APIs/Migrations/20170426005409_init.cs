﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Almacervices.APIs.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categoria",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Detalle = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categoria", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Pais",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Codigo = table.Column<string>(maxLength: 10, nullable: false),
                    Descripcion = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "perfil_usuario",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Descripcion = table.Column<string>(maxLength: 100, nullable: false),
                    Perfil = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_perfil_usuario", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoMargen",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Codigo = table.Column<string>(nullable: false),
                    Detalle = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoMargen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "unidad_medida",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Codigo = table.Column<string>(nullable: false),
                    Detalle = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_unidad_medida", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    ApellidoMaterno = table.Column<string>(maxLength: 50, nullable: true),
                    ApellidoPaterno = table.Column<string>(maxLength: 50, nullable: false),
                    Dv = table.Column<char>(nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    EmailConfirmado = table.Column<bool>(nullable: false),
                    EstaEliminado = table.Column<bool>(nullable: false),
                    EstaSuspendido = table.Column<bool>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    FechaEliminacion = table.Column<DateTime>(nullable: false),
                    FechaModificacion = table.Column<DateTime>(nullable: false),
                    FechaSuspension = table.Column<DateTime>(nullable: false),
                    FechaUltimoLogin = table.Column<DateTime>(nullable: false),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Password = table.Column<string>(nullable: false),
                    Rut = table.Column<int>(maxLength: 8, nullable: false),
                    Salt = table.Column<byte[]>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Region",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Descripcion = table.Column<string>(maxLength: 100, nullable: false),
                    PaisId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Region", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Region_Pais_PaisId",
                        column: x => x.PaisId,
                        principalTable: "Pais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Producto",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Codigo = table.Column<long>(maxLength: 13, nullable: false),
                    Detalle = table.Column<string>(maxLength: 50, nullable: false),
                    EstaVerificado = table.Column<bool>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    Marca = table.Column<string>(nullable: true),
                    Peso = table.Column<double>(nullable: false),
                    UnidadMedidaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Producto", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Producto_unidad_medida_UnidadMedidaId",
                        column: x => x.UnidadMedidaId,
                        principalTable: "unidad_medida",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Provincia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Descripcion = table.Column<string>(maxLength: 100, nullable: false),
                    RegionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provincia", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Provincia_Region_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Region",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comuna",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Descripcion = table.Column<string>(maxLength: 100, nullable: false),
                    ProvinciaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comuna", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comuna_Provincia_ProvinciaId",
                        column: x => x.ProvinciaId,
                        principalTable: "Provincia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Negocio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    ComunaFacturacionId = table.Column<int>(nullable: false),
                    DireccionFacturacion = table.Column<string>(maxLength: 100, nullable: false),
                    Dv = table.Column<char>(maxLength: 1, nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    EstaEliminado = table.Column<bool>(nullable: false),
                    EstaSuspendido = table.Column<bool>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    FechaEliminacion = table.Column<DateTime>(nullable: false),
                    FechaModificacion = table.Column<DateTime>(nullable: false),
                    FechaSuspension = table.Column<DateTime>(nullable: false),
                    GiroComercial = table.Column<string>(maxLength: 400, nullable: false),
                    NombreContactoDos = table.Column<string>(maxLength: 100, nullable: true),
                    NombreContactoUno = table.Column<string>(maxLength: 100, nullable: false),
                    PaisId = table.Column<int>(nullable: false),
                    PermiteCreditoClientes = table.Column<bool>(nullable: false),
                    ProvinciaFacturacionId = table.Column<int>(nullable: false),
                    RazonSocial = table.Column<string>(maxLength: 200, nullable: false),
                    RegionFacturacionId = table.Column<int>(nullable: false),
                    Rut = table.Column<int>(maxLength: 8, nullable: false),
                    Telefono = table.Column<int>(nullable: false),
                    TelefonoContactoDos = table.Column<int>(nullable: false),
                    TelefonoContactoUno = table.Column<int>(nullable: false),
                    UsaDTE = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Negocio", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Negocio_Comuna_ComunaFacturacionId",
                        column: x => x.ComunaFacturacionId,
                        principalTable: "Comuna",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Negocio_Pais_PaisId",
                        column: x => x.PaisId,
                        principalTable: "Pais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Negocio_Provincia_ProvinciaFacturacionId",
                        column: x => x.ProvinciaFacturacionId,
                        principalTable: "Provincia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Negocio_Region_RegionFacturacionId",
                        column: x => x.RegionFacturacionId,
                        principalTable: "Region",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sucursal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    ComunaId = table.Column<int>(nullable: false),
                    Direccion = table.Column<string>(maxLength: 100, nullable: false),
                    EsMatriz = table.Column<bool>(nullable: false),
                    EstaEliminado = table.Column<bool>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    FechaEliminacion = table.Column<DateTime>(nullable: false),
                    FechaModificacion = table.Column<DateTime>(nullable: false),
                    NegocioId = table.Column<int>(nullable: false),
                    Nombre = table.Column<string>(nullable: true),
                    PaisId = table.Column<int>(nullable: false),
                    ProvinciaId = table.Column<int>(nullable: false),
                    RegionId = table.Column<int>(nullable: false),
                    Telefono = table.Column<int>(nullable: false),
                    UsuarioModificador = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sucursal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sucursal_Comuna_ComunaId",
                        column: x => x.ComunaId,
                        principalTable: "Comuna",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sucursal_Negocio_NegocioId",
                        column: x => x.NegocioId,
                        principalTable: "Negocio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sucursal_Pais_PaisId",
                        column: x => x.PaisId,
                        principalTable: "Pais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sucursal_Provincia_ProvinciaId",
                        column: x => x.ProvinciaId,
                        principalTable: "Provincia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sucursal_Region_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Region",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "acceso_sucursal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    EstaEliminado = table.Column<bool>(nullable: false),
                    FechaEliminacion = table.Column<DateTime>(nullable: false),
                    PerfilId = table.Column<int>(nullable: false),
                    SucursalId = table.Column<int>(nullable: false),
                    UsuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_acceso_sucursal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_acceso_sucursal_perfil_usuario_PerfilId",
                        column: x => x.PerfilId,
                        principalTable: "perfil_usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_acceso_sucursal_Sucursal_SucursalId",
                        column: x => x.SucursalId,
                        principalTable: "Sucursal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_acceso_sucursal_Usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cliente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    ApellidoMaterno = table.Column<string>(maxLength: 50, nullable: true),
                    ApellidoPaterno = table.Column<string>(maxLength: 50, nullable: false),
                    Dv = table.Column<char>(nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    EmailConfirmado = table.Column<bool>(nullable: false),
                    EstaEliminado = table.Column<bool>(nullable: false),
                    EstaSuspendido = table.Column<bool>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    FechaEliminacion = table.Column<DateTime>(nullable: false),
                    FechaModificacion = table.Column<DateTime>(nullable: false),
                    FechaSuspension = table.Column<DateTime>(nullable: false),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Rut = table.Column<string>(maxLength: 8, nullable: false),
                    SucursalId = table.Column<int>(nullable: false),
                    Usuario = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cliente", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cliente_Sucursal_SucursalId",
                        column: x => x.SucursalId,
                        principalTable: "Sucursal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Proveedor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    CargoContacto = table.Column<string>(maxLength: 50, nullable: true),
                    CelularContacto = table.Column<int>(nullable: false),
                    ComunaId = table.Column<int>(nullable: false),
                    Direccion = table.Column<string>(maxLength: 100, nullable: true),
                    Dv = table.Column<char>(nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: true),
                    EstaEliminado = table.Column<bool>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    FechaEliminacion = table.Column<DateTime>(nullable: false),
                    FechaModificacion = table.Column<DateTime>(nullable: false),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    NombreContacto = table.Column<string>(maxLength: 50, nullable: true),
                    PaisId = table.Column<int>(nullable: false),
                    ProvinciaId = table.Column<int>(nullable: false),
                    RegionId = table.Column<int>(nullable: false),
                    Rut = table.Column<int>(maxLength: 8, nullable: false),
                    SitioWeb = table.Column<string>(maxLength: 50, nullable: true),
                    SucursalId = table.Column<int>(nullable: false),
                    TelefonoContacto = table.Column<int>(nullable: false),
                    UsuarioModificador = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proveedor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Proveedor_Comuna_ComunaId",
                        column: x => x.ComunaId,
                        principalTable: "Comuna",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Proveedor_Pais_PaisId",
                        column: x => x.PaisId,
                        principalTable: "Pais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Proveedor_Provincia_ProvinciaId",
                        column: x => x.ProvinciaId,
                        principalTable: "Provincia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Proveedor_Region_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Region",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Proveedor_Sucursal_SucursalId",
                        column: x => x.SucursalId,
                        principalTable: "Sucursal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "producto_por_sucursal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    CategoriaId = table.Column<int>(nullable: false),
                    Codigo = table.Column<long>(maxLength: 13, nullable: false),
                    Costo = table.Column<double>(nullable: false),
                    Detalle = table.Column<string>(maxLength: 50, nullable: false),
                    EstaEliminado = table.Column<bool>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    FechaEliminacion = table.Column<DateTime>(nullable: false),
                    FechaModificacion = table.Column<DateTime>(nullable: false),
                    Ganancia = table.Column<double>(nullable: false),
                    Marca = table.Column<string>(maxLength: 50, nullable: true),
                    Margen = table.Column<double>(nullable: false),
                    NivelStock = table.Column<int>(nullable: false),
                    Peso = table.Column<double>(nullable: false),
                    Precio = table.Column<double>(nullable: false),
                    ProductoId = table.Column<int>(nullable: true),
                    ProveedorId = table.Column<int>(nullable: false),
                    StockCritico = table.Column<int>(nullable: false),
                    SucursalId = table.Column<int>(nullable: false),
                    TipoMargenId = table.Column<int>(nullable: false),
                    UnidadMedidaId = table.Column<int>(nullable: false),
                    UsuarioModificador = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_producto_por_sucursal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_producto_por_sucursal_Categoria_CategoriaId",
                        column: x => x.CategoriaId,
                        principalTable: "Categoria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_producto_por_sucursal_Producto_ProductoId",
                        column: x => x.ProductoId,
                        principalTable: "Producto",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_producto_por_sucursal_Proveedor_ProveedorId",
                        column: x => x.ProveedorId,
                        principalTable: "Proveedor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_producto_por_sucursal_Sucursal_SucursalId",
                        column: x => x.SucursalId,
                        principalTable: "Sucursal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_producto_por_sucursal_TipoMargen_TipoMargenId",
                        column: x => x.TipoMargenId,
                        principalTable: "TipoMargen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_producto_por_sucursal_unidad_medida_UnidadMedidaId",
                        column: x => x.UnidadMedidaId,
                        principalTable: "unidad_medida",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_acceso_sucursal_PerfilId",
                table: "acceso_sucursal",
                column: "PerfilId");

            migrationBuilder.CreateIndex(
                name: "IX_acceso_sucursal_UsuarioId",
                table: "acceso_sucursal",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_acceso_sucursal_SucursalId_UsuarioId_PerfilId",
                table: "acceso_sucursal",
                columns: new[] { "SucursalId", "UsuarioId", "PerfilId" });

            migrationBuilder.CreateIndex(
                name: "IX_Cliente_Rut",
                table: "Cliente",
                column: "Rut");

            migrationBuilder.CreateIndex(
                name: "IX_Cliente_SucursalId",
                table: "Cliente",
                column: "SucursalId");

            migrationBuilder.CreateIndex(
                name: "IX_Comuna_ProvinciaId",
                table: "Comuna",
                column: "ProvinciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Negocio_ComunaFacturacionId",
                table: "Negocio",
                column: "ComunaFacturacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Negocio_PaisId",
                table: "Negocio",
                column: "PaisId");

            migrationBuilder.CreateIndex(
                name: "IX_Negocio_ProvinciaFacturacionId",
                table: "Negocio",
                column: "ProvinciaFacturacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Negocio_RegionFacturacionId",
                table: "Negocio",
                column: "RegionFacturacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Negocio_Rut",
                table: "Negocio",
                column: "Rut");

            migrationBuilder.CreateIndex(
                name: "IX_Producto_UnidadMedidaId",
                table: "Producto",
                column: "UnidadMedidaId");

            migrationBuilder.CreateIndex(
                name: "IX_producto_por_sucursal_CategoriaId",
                table: "producto_por_sucursal",
                column: "CategoriaId");

            migrationBuilder.CreateIndex(
                name: "IX_producto_por_sucursal_Codigo",
                table: "producto_por_sucursal",
                column: "Codigo");

            migrationBuilder.CreateIndex(
                name: "IX_producto_por_sucursal_ProductoId",
                table: "producto_por_sucursal",
                column: "ProductoId");

            migrationBuilder.CreateIndex(
                name: "IX_producto_por_sucursal_ProveedorId",
                table: "producto_por_sucursal",
                column: "ProveedorId");

            migrationBuilder.CreateIndex(
                name: "IX_producto_por_sucursal_SucursalId",
                table: "producto_por_sucursal",
                column: "SucursalId");

            migrationBuilder.CreateIndex(
                name: "IX_producto_por_sucursal_TipoMargenId",
                table: "producto_por_sucursal",
                column: "TipoMargenId");

            migrationBuilder.CreateIndex(
                name: "IX_producto_por_sucursal_UnidadMedidaId",
                table: "producto_por_sucursal",
                column: "UnidadMedidaId");

            migrationBuilder.CreateIndex(
                name: "IX_Proveedor_ComunaId",
                table: "Proveedor",
                column: "ComunaId");

            migrationBuilder.CreateIndex(
                name: "IX_Proveedor_PaisId",
                table: "Proveedor",
                column: "PaisId");

            migrationBuilder.CreateIndex(
                name: "IX_Proveedor_ProvinciaId",
                table: "Proveedor",
                column: "ProvinciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Proveedor_RegionId",
                table: "Proveedor",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Proveedor_SucursalId",
                table: "Proveedor",
                column: "SucursalId");

            migrationBuilder.CreateIndex(
                name: "IX_Provincia_RegionId",
                table: "Provincia",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Region_PaisId",
                table: "Region",
                column: "PaisId");

            migrationBuilder.CreateIndex(
                name: "IX_Sucursal_ComunaId",
                table: "Sucursal",
                column: "ComunaId");

            migrationBuilder.CreateIndex(
                name: "IX_Sucursal_NegocioId",
                table: "Sucursal",
                column: "NegocioId");

            migrationBuilder.CreateIndex(
                name: "IX_Sucursal_PaisId",
                table: "Sucursal",
                column: "PaisId");

            migrationBuilder.CreateIndex(
                name: "IX_Sucursal_ProvinciaId",
                table: "Sucursal",
                column: "ProvinciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Sucursal_RegionId",
                table: "Sucursal",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Usuario_Rut",
                table: "Usuario",
                column: "Rut");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "acceso_sucursal");

            migrationBuilder.DropTable(
                name: "Cliente");

            migrationBuilder.DropTable(
                name: "producto_por_sucursal");

            migrationBuilder.DropTable(
                name: "perfil_usuario");

            migrationBuilder.DropTable(
                name: "Usuario");

            migrationBuilder.DropTable(
                name: "Categoria");

            migrationBuilder.DropTable(
                name: "Producto");

            migrationBuilder.DropTable(
                name: "Proveedor");

            migrationBuilder.DropTable(
                name: "TipoMargen");

            migrationBuilder.DropTable(
                name: "unidad_medida");

            migrationBuilder.DropTable(
                name: "Sucursal");

            migrationBuilder.DropTable(
                name: "Negocio");

            migrationBuilder.DropTable(
                name: "Comuna");

            migrationBuilder.DropTable(
                name: "Provincia");

            migrationBuilder.DropTable(
                name: "Region");

            migrationBuilder.DropTable(
                name: "Pais");
        }
    }
}
