﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Almacervice.Data;

namespace Almacervices.APIs.Migrations
{
    [DbContext(typeof(AlmacerviceContext))]
    partial class AlmacerviceContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("Almacervice.Model.Entities.AccesoSucursal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("EstaEliminado");

                    b.Property<DateTime>("FechaEliminacion");

                    b.Property<int>("PerfilId");

                    b.Property<int>("SucursalId");

                    b.Property<int>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("PerfilId");

                    b.HasIndex("UsuarioId");

                    b.HasIndex("SucursalId", "UsuarioId", "PerfilId");

                    b.ToTable("acceso_sucursal");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Categoria", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Detalle")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Categoria");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Cliente", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApellidoMaterno")
                        .HasMaxLength(50);

                    b.Property<string>("ApellidoPaterno")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<char>("Dv");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<bool>("EmailConfirmado");

                    b.Property<bool>("EstaEliminado");

                    b.Property<bool>("EstaSuspendido");

                    b.Property<DateTime>("FechaCreacion");

                    b.Property<DateTime>("FechaEliminacion");

                    b.Property<DateTime>("FechaModificacion");

                    b.Property<DateTime>("FechaSuspension");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Rut")
                        .IsRequired()
                        .HasMaxLength(8);

                    b.Property<int>("SucursalId");

                    b.Property<string>("Usuario")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("Rut");

                    b.HasIndex("SucursalId");

                    b.ToTable("Cliente");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Comuna", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("ProvinciaId");

                    b.HasKey("Id");

                    b.HasIndex("ProvinciaId");

                    b.ToTable("Comuna");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Negocio", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ComunaFacturacionId");

                    b.Property<string>("DireccionFacturacion")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<char>("Dv")
                        .HasMaxLength(1);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<bool>("EstaEliminado");

                    b.Property<bool>("EstaSuspendido");

                    b.Property<DateTime>("FechaCreacion");

                    b.Property<DateTime>("FechaEliminacion");

                    b.Property<DateTime>("FechaModificacion");

                    b.Property<DateTime>("FechaSuspension");

                    b.Property<string>("GiroComercial")
                        .IsRequired()
                        .HasMaxLength(400);

                    b.Property<string>("NombreContactoDos")
                        .HasMaxLength(100);

                    b.Property<string>("NombreContactoUno")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("PaisId");

                    b.Property<bool>("PermiteCreditoClientes");

                    b.Property<int>("ProvinciaFacturacionId");

                    b.Property<string>("RazonSocial")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<int>("RegionFacturacionId");

                    b.Property<int>("Rut")
                        .HasMaxLength(8);

                    b.Property<int>("Telefono");

                    b.Property<int>("TelefonoContactoDos");

                    b.Property<int>("TelefonoContactoUno");

                    b.Property<bool>("UsaDTE");

                    b.HasKey("Id");

                    b.HasIndex("ComunaFacturacionId");

                    b.HasIndex("PaisId");

                    b.HasIndex("ProvinciaFacturacionId");

                    b.HasIndex("RegionFacturacionId");

                    b.HasIndex("Rut");

                    b.ToTable("Negocio");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Pais", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Codigo")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.ToTable("Pais");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.PerfilUsuario", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Perfil")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("perfil_usuario");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Producto", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("Codigo")
                        .HasMaxLength(13);

                    b.Property<string>("Detalle")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<bool>("EstaVerificado");

                    b.Property<DateTime>("FechaCreacion");

                    b.Property<string>("Marca");

                    b.Property<double>("Peso");

                    b.Property<int>("UnidadMedidaId");

                    b.HasKey("Id");

                    b.HasIndex("UnidadMedidaId");

                    b.ToTable("Producto");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.ProductoSucursal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoriaId");

                    b.Property<long>("Codigo")
                        .HasMaxLength(13);

                    b.Property<double>("Costo");

                    b.Property<string>("Detalle")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<bool>("EstaEliminado");

                    b.Property<DateTime>("FechaCreacion");

                    b.Property<DateTime>("FechaEliminacion");

                    b.Property<DateTime>("FechaModificacion");

                    b.Property<double>("Ganancia");

                    b.Property<string>("Marca")
                        .HasMaxLength(50);

                    b.Property<double>("Margen");

                    b.Property<int>("NivelStock");

                    b.Property<double>("Peso");

                    b.Property<double>("Precio");

                    b.Property<int?>("ProductoId");

                    b.Property<int>("ProveedorId");

                    b.Property<int>("StockCritico");

                    b.Property<int>("SucursalId");

                    b.Property<int>("TipoMargenId");

                    b.Property<int>("UnidadMedidaId");

                    b.Property<string>("UsuarioModificador")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CategoriaId");

                    b.HasIndex("Codigo");

                    b.HasIndex("ProductoId");

                    b.HasIndex("ProveedorId");

                    b.HasIndex("SucursalId");

                    b.HasIndex("TipoMargenId");

                    b.HasIndex("UnidadMedidaId");

                    b.ToTable("producto_por_sucursal");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Proveedor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CargoContacto")
                        .HasMaxLength(50);

                    b.Property<int>("CelularContacto");

                    b.Property<int>("ComunaId");

                    b.Property<string>("Direccion")
                        .HasMaxLength(100);

                    b.Property<char>("Dv");

                    b.Property<string>("Email")
                        .HasMaxLength(100);

                    b.Property<bool>("EstaEliminado");

                    b.Property<DateTime>("FechaCreacion");

                    b.Property<DateTime>("FechaEliminacion");

                    b.Property<DateTime>("FechaModificacion");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("NombreContacto")
                        .HasMaxLength(50);

                    b.Property<int>("PaisId");

                    b.Property<int>("ProvinciaId");

                    b.Property<int>("RegionId");

                    b.Property<int>("Rut")
                        .HasMaxLength(8);

                    b.Property<string>("SitioWeb")
                        .HasMaxLength(50);

                    b.Property<int>("SucursalId");

                    b.Property<int>("TelefonoContacto");

                    b.Property<string>("UsuarioModificador")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("ComunaId");

                    b.HasIndex("PaisId");

                    b.HasIndex("ProvinciaId");

                    b.HasIndex("RegionId");

                    b.HasIndex("SucursalId");

                    b.ToTable("Proveedor");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Provincia", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("RegionId");

                    b.HasKey("Id");

                    b.HasIndex("RegionId");

                    b.ToTable("Provincia");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Region", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("PaisId");

                    b.HasKey("Id");

                    b.HasIndex("PaisId");

                    b.ToTable("Region");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Sucursal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ComunaId");

                    b.Property<string>("Direccion")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<bool>("EsMatriz");

                    b.Property<bool>("EstaEliminado");

                    b.Property<DateTime>("FechaCreacion");

                    b.Property<DateTime>("FechaEliminacion");

                    b.Property<DateTime>("FechaModificacion");

                    b.Property<int>("NegocioId");

                    b.Property<string>("Nombre");

                    b.Property<int>("PaisId");

                    b.Property<int>("ProvinciaId");

                    b.Property<int>("RegionId");

                    b.Property<int>("Telefono");

                    b.Property<string>("UsuarioModificador");

                    b.HasKey("Id");

                    b.HasIndex("ComunaId");

                    b.HasIndex("NegocioId");

                    b.HasIndex("PaisId");

                    b.HasIndex("ProvinciaId");

                    b.HasIndex("RegionId");

                    b.ToTable("Sucursal");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.TipoMargen", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Codigo")
                        .IsRequired();

                    b.Property<string>("Detalle")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("TipoMargen");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.UnidadMedida", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Codigo")
                        .IsRequired();

                    b.Property<string>("Detalle")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("unidad_medida");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Usuario", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApellidoMaterno")
                        .HasMaxLength(50);

                    b.Property<string>("ApellidoPaterno")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<char>("Dv");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<bool>("EmailConfirmado");

                    b.Property<bool>("EstaEliminado");

                    b.Property<bool>("EstaSuspendido");

                    b.Property<DateTime>("FechaCreacion");

                    b.Property<DateTime>("FechaEliminacion");

                    b.Property<DateTime>("FechaModificacion");

                    b.Property<DateTime>("FechaSuspension");

                    b.Property<DateTime>("FechaUltimoLogin");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<int>("Rut")
                        .HasMaxLength(8);

                    b.Property<byte[]>("Salt")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("Rut");

                    b.ToTable("Usuario");
                });

            modelBuilder.Entity("Almacervice.Model.Entities.AccesoSucursal", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.PerfilUsuario", "Perfil")
                        .WithMany("AccesoSucursales")
                        .HasForeignKey("PerfilId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Sucursal", "Sucursal")
                        .WithMany("AccesoSucursales")
                        .HasForeignKey("SucursalId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Usuario", "Usuario")
                        .WithMany("AccesoSucursales")
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Cliente", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.Sucursal", "Sucursal")
                        .WithMany("Clientes")
                        .HasForeignKey("SucursalId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Comuna", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.Provincia", "Provincia")
                        .WithMany("Comunas")
                        .HasForeignKey("ProvinciaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Negocio", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.Comuna", "Comuna")
                        .WithMany("Negocios")
                        .HasForeignKey("ComunaFacturacionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Pais", "Pais")
                        .WithMany("Negocios")
                        .HasForeignKey("PaisId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Provincia", "Provincia")
                        .WithMany("Negocios")
                        .HasForeignKey("ProvinciaFacturacionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Region", "Region")
                        .WithMany("Negocios")
                        .HasForeignKey("RegionFacturacionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Producto", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.UnidadMedida", "UnidadMedida")
                        .WithMany("Productos")
                        .HasForeignKey("UnidadMedidaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Almacervice.Model.Entities.ProductoSucursal", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.Categoria", "Categoria")
                        .WithMany("ProductosSucursal")
                        .HasForeignKey("CategoriaId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Producto", "Producto")
                        .WithMany("ProductosSucursal")
                        .HasForeignKey("ProductoId");

                    b.HasOne("Almacervice.Model.Entities.Proveedor", "Proveedor")
                        .WithMany("ProductosSucursal")
                        .HasForeignKey("ProveedorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Sucursal", "Sucursal")
                        .WithMany("ProductosSucursal")
                        .HasForeignKey("SucursalId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.TipoMargen", "TipoMargen")
                        .WithMany("ProductosSucursal")
                        .HasForeignKey("TipoMargenId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.UnidadMedida", "UnidadMedida")
                        .WithMany("ProductosSucursal")
                        .HasForeignKey("UnidadMedidaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Proveedor", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.Comuna", "Comuna")
                        .WithMany("Proveedores")
                        .HasForeignKey("ComunaId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Pais", "Pais")
                        .WithMany("Proveedores")
                        .HasForeignKey("PaisId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Provincia", "Provincia")
                        .WithMany("Proveedores")
                        .HasForeignKey("ProvinciaId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Region", "Region")
                        .WithMany("Proveedores")
                        .HasForeignKey("RegionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Sucursal", "Sucursal")
                        .WithMany("Proveedores")
                        .HasForeignKey("SucursalId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Provincia", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.Region", "Region")
                        .WithMany("Provincias")
                        .HasForeignKey("RegionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Region", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.Pais", "Pais")
                        .WithMany("Regiones")
                        .HasForeignKey("PaisId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Almacervice.Model.Entities.Sucursal", b =>
                {
                    b.HasOne("Almacervice.Model.Entities.Comuna", "Comuna")
                        .WithMany("Sucursales")
                        .HasForeignKey("ComunaId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Negocio", "Negocio")
                        .WithMany("Sucursales")
                        .HasForeignKey("NegocioId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Pais", "Pais")
                        .WithMany("Sucursales")
                        .HasForeignKey("PaisId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Provincia", "Provincia")
                        .WithMany("Sucursales")
                        .HasForeignKey("ProvinciaId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Almacervice.Model.Entities.Region", "Region")
                        .WithMany("Sucursales")
                        .HasForeignKey("RegionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
