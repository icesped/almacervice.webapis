﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.Core.Utils
{
    public class RutUtil
    {
        /// <summary>
        /// Verifica que un rut sea válido
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        public static string[] GetRutDvValidated(string rut)
        {
            int dvAux;
            int counter;
            int multiple;
            int accumulator;
            string dvStr;
            string[] rutAndDv = new string[2];

            rut = rut.ToUpper().Trim();
            rut = rut.Replace(".", "");
            rut = rut.Replace("-", "");

            string rutStr = rut.Substring(0, rut.Length - 1);
            int rutInt = int.Parse(rutStr);
            char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

            counter = 2;
            accumulator = 0;

            while (rutInt != 0)
            {
                multiple = (rutInt % 10) * counter;
                accumulator = accumulator + multiple;
                rutInt = rutInt / 10;
                counter = counter + 1;

                if (counter == 8)
                    counter = 2;
            }

            dvAux = 11 - (accumulator % 11);
            dvStr = dvAux.ToString().Trim();

            if (dvAux == 10)
                dvStr = "K";

            if (dvAux == 11)
                dvStr = "0";

            if (dvStr.ToString() == dv.ToString())
            {
                rutAndDv[0] = rutStr.ToString();
                rutAndDv[1] = dvStr;
            }
            else
                return null;

            return rutAndDv;
        }
    }
}
