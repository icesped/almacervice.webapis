﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Security.Cryptography;

namespace Almacervices.APIs.Core.Utils
{
    public class PasswordUtil
    {
        /// <summary>
        /// Obtiene la contraseña hasheada junto al salt
        /// </summary>
        /// <param name="pass"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static string GetHashWithSalt(string pass, byte[] salt)
        {
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: pass,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));

            return hashed;
        }

        /// <summary>
        /// Valida que la contraseña recibida sea igual a la constraseña almacenada
        /// </summary>
        /// <param name="pass"></param>
        /// <param name="userHash"></param>
        /// <param name="userSalt"></param>
        /// <returns></returns>
        public static bool ValidatePassword(string pass, string userHash, byte[] userSalt)
        {
            string expectedHash = GetHashWithSalt(pass, userSalt);

            if (!string.Equals(userHash, expectedHash))
                return false;

            return true;
        }

        /// <summary>
        /// Crea un nuevo Salt aleatorio
        /// </summary>
        /// <returns></returns>
        public static byte[] GetSalt()
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }
    }
}
