﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.Core.Enum
{
    public enum ErrorCodeEnum
    {
        InvalidRutError = 400,
        BDUnavailableError = 503,

    }
}
