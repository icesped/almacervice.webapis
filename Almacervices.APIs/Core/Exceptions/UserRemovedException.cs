﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.Core.Exceptions
{
    public class UserRemovedException : Exception
    {
        public UserRemovedException() {}

        public UserRemovedException(string message) : base(message) { }

        public UserRemovedException(string message, Exception inner) : base(message, inner) { }
    }
}
