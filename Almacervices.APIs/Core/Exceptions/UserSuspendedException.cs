﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.Core.Exceptions
{
    public class UserSuspendedException : Exception
    {
        public UserSuspendedException() { }

        public UserSuspendedException(string message) : base(message) { }

        public UserSuspendedException(string message, Exception inner) : base(message, inner) { }
    }
}
