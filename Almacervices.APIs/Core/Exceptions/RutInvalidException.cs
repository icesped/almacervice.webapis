﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.Core.Exceptions
{
    public class RutInvalidException : Exception
    {
        public RutInvalidException() { }

        public RutInvalidException(string message) : base(message) { }

        public RutInvalidException(string message, Exception inner) : base(message, inner) { }
    }
}
