﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.ViewModels
{
    public class ProveedorDTO
    {
        public int Id { get; set; }
        public int SucursalId { get; set; }
        public int Rut { get; set; }
        public char Dv { get; set; }
        public string Nombre { get; set; }
        public string NombreContacto { get; set; }
        public string CargoContacto { get; set; }
        public int TelefonoContacto { get; set; }
        public int CelularContacto { get; set; }
        public string Email { get; set; }
        public string SitioWeb { get; set; }
        public string Direccion { get; set; }
        public int PaisId { get; set; }
        public int RegionId { get; set; }
        public int ProvinciaId { get; set; }
        public int ComunaId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioModificador { get; set; }
        public bool EstaEliminado { get; set; }
        public DateTime FechaEliminacion { get; set; }
    }
}
