﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.ViewModels
{
    public class CategoriaDTO
    {
        public int Id { get; set; }
        public string Detalle { get; set; }
    }
}
