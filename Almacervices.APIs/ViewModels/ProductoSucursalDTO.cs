﻿using System;

namespace Almacervices.APIs.ViewModels
{
    public class ProductoSucursalDTO
    {
        public int Id { get; set; }
        public int SucursalId { get; set; }
        public long Codigo { get; set; }
        public string Detalle { get; set; }
        public string Marca { get; set; }
        public double Peso { get; set; }
        public int UnidadMedidaId { get; set; }
        public int CategoriaId { get; set; }
        public int ProveedorId { get; set; }
        public int StockCritico { get; set; }
        public int NivelStock { get; set; }
        public double Costo { get; set; }
        public double Margen { get; set; }
        public int TipoMargenId { get; set; }
        public double Ganancia { get; set; }
        public double Precio { get; set; }
        public double Cantidad { get; set; }
        public double Total { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool EstaEliminado { get; set; }
        public DateTime FechaEliminacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioModificador { get; set; }
    }
}
