﻿using Almacervice.Model.Entities;
using AutoMapper;


namespace Almacervices.APIs.ViewModels.Mappings
{
    public class DtoToModelMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<PaisDTO, Pais>();
            Mapper.CreateMap<UsuarioDTO, Usuario>();
            Mapper.CreateMap<NegocioDTO, Negocio>();
            Mapper.CreateMap<SucursalDTO, Sucursal>();
            Mapper.CreateMap<ProveedorDTO, Proveedor>();
            Mapper.CreateMap<ProductoDTO, Producto>();
            Mapper.CreateMap<ProductoSucursalDTO, ProductoSucursal>();
            Mapper.CreateMap<CategoriaDTO, Categoria>();
            Mapper.CreateMap<UnidadMedidaDTO, UnidadMedida>();
            Mapper.CreateMap<TipoMargenDTO, TipoMargen>();
        }
    }
}
