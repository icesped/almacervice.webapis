﻿using Almacervice.Model.Entities;
using AutoMapper;

namespace Almacervices.APIs.ViewModels.Mappings
{
    public class ModelToDtoMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Pais, PaisDTO>();
            Mapper.CreateMap<Usuario, UsuarioDTO>();
            Mapper.CreateMap<Negocio, NegocioDTO>();
            Mapper.CreateMap<Sucursal, SucursalDTO>();
            Mapper.CreateMap<Proveedor, ProveedorDTO>();
            Mapper.CreateMap<Producto, ProductoDTO>();
            Mapper.CreateMap<ProductoSucursal, ProductoSucursalDTO>();
            Mapper.CreateMap<Categoria, CategoriaDTO>();
            Mapper.CreateMap<UnidadMedida, UnidadMedidaDTO>();
            Mapper.CreateMap<TipoMargen, TipoMargenDTO>();
        }
    }
}
