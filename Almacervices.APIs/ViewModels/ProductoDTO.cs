﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.ViewModels
{
    public class ProductoDTO
    {
        public int Id { get; set; }
        public long Codigo { get; set; }
        public string Detalle { get; set; }
        public string Marca { get; set; }
        public double Peso { get; set; }
        public int UnidadMedidaId { get; set; }
        public bool EstaVerificado { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
