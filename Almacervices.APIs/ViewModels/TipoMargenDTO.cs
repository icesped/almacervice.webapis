﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.ViewModels
{
    public class TipoMargenDTO
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Detalle { get; set; }
    }
}
