﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.ViewModels
{
    public class NegocioDTO
    {
        public int Id { get; set; }
        public int Rut { get; set; }
        public char Dv { get; set; }
        public string RazonSocial { get; set; }
        public string DireccionFacturacion { get; set; }
        public int ComunaFacturacionId { get; set; }
        public int ProvinciaFacturacionId { get; set; }
        public int RegionFacturacionId { get; set; }
        public int PaisId { get; set; }
        public string GiroComercial { get; set; }
        public int Telefono { get; set; }
        public string Email { get; set; }
        public string NombreContactoUno { get; set; }
        public int TelefonoContactoUno { get; set; }
        public string NombreContactoDos { get; set; }
        public int TelefonoContactoDos { get; set; }
        public bool UsaDTE { get; set; }
        public bool PermiteCreditoClientes { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public bool EstaSuspendido { get; set; }
        public DateTime FechaSuspension { get; set; }
        public bool EstaEliminado { get; set; }
        public DateTime FechaEliminacion { get; set; }
    }
}
