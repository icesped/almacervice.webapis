﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.ViewModels
{
    public class UsuarioDTO
    {
        public int Id { get; set; }
        public int Rut { get; set; }
        public char Dv { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmado { get; set; }
        public string Password { get; set; }
        public Byte[] Salt { get; set; }
        public DateTime FechaUltimoLogin { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public bool EstaSuspendido { get; set; }
        public DateTime FechaSuspension { get; set; }
        public bool EstaEliminado { get; set; }
        public DateTime FechaEliminacion { get; set; }
    }
}
