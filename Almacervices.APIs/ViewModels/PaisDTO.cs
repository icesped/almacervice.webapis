﻿namespace Almacervices.APIs.ViewModels
{
    public class PaisDTO
    {
        public int Id { set; get; }
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
    }
}
