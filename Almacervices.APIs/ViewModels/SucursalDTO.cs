﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervices.APIs.ViewModels
{
    public class SucursalDTO
    {
        public int Id { get; set; }
        public int NegocioId { get; set; }
        public bool EsMatriz { get; set; }
        public string Nombre { get; set; }
        public int Telefono { get; set; }
        public string Direccion { get; set; }
        public int ComunaId { get; set; }
        public int ProvinciaId { get; set; }
        public int RegionId { get; set; }
        public int PaisId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioModificador { get; set; }
        public bool EstaEliminado { get; set; }
        public DateTime FechaEliminacion { get; set; }
    }
}
