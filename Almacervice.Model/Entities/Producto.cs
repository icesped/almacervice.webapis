﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class Producto : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(13)]
        public long Codigo { get; set; }
        [Required, MaxLength(50)]
        public string Detalle { get; set; }
        public string Marca { get; set; }
        public double Peso { get; set; }
        public int UnidadMedidaId { get; set; }
        [Required]
        public bool EstaVerificado { get; set; }
        [Required]
        public DateTime FechaCreacion { get; set; }

        /* Relaciones */
        public UnidadMedida UnidadMedida { get; set; }
        public List<ProductoSucursal> ProductosSucursal { get; set; }
    }
}
