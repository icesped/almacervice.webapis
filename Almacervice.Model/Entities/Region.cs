﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Almacervice.Model.Entities
{
    public class Region : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(100)]
        public string Descripcion { get; set; }

        /* Foreign Keys */
        public int PaisId { get; set; }

        /* Relaciones */
        public Pais Pais { get; set; }
        public List<Negocio> Negocios { get; set; }
        public List<Sucursal> Sucursales { get; set; }
        public List<Provincia> Provincias { get; set; }
        public List<Proveedor> Proveedores { get; set; }
    }
}
