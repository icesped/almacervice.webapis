﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class Comuna : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(100)]
        public string Descripcion { get; set; }
        public int ProvinciaId { get; set; }

        /* Relaciones */
        public Provincia Provincia { get; set; }
        public List<Negocio> Negocios { get; set; }
        public List<Sucursal> Sucursales { get; set; }
        public List<Proveedor> Proveedores { get; set; }
    }
}
