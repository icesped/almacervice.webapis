﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class UnidadMedida: IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Codigo { get; set; }
        [Required]
        public string Detalle { get; set; } 

        public List<Producto> Productos { get; set; }
        public List<ProductoSucursal> ProductosSucursal { get; set; }
    }
}
