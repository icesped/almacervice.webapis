﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class Negocio : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(8)]
        public int Rut { get; set; }
        [Required, MaxLength(1)]
        public char Dv { get; set; }
        [Required, MaxLength(200)]
        public string RazonSocial { get; set; }
        [Required, MaxLength(100)]
        public string DireccionFacturacion { get; set; }
        [Required]
        public int ComunaFacturacionId { get; set; }
        [Required]
        public int ProvinciaFacturacionId { get; set; }
        [Required]
        public int RegionFacturacionId { get; set; }
        [Required]
        public int PaisId { get; set; }
        [Required, MaxLength(400)]
        public string GiroComercial { get; set; }
        [Required]
        public int Telefono { get; set; }
        [Required, MaxLength(100)]
        public string Email { get; set; }
        [Required, MaxLength(100)]
        public string NombreContactoUno { get; set; }
        [Required]
        public int TelefonoContactoUno { get; set; }
        [MaxLength(100)]
        public string NombreContactoDos { get; set; }
        public int TelefonoContactoDos { get; set; }
        [Required]
        public bool UsaDTE { get; set; }
        [Required]
        public bool PermiteCreditoClientes { get; set; }
        [Required]
        public DateTime FechaCreacion { get; set; }
        [Required]
        public DateTime FechaModificacion { get; set; }
        [Required]
        public bool EstaSuspendido { get; set; }
        public DateTime FechaSuspension { get; set; }
        [Required]
        public bool EstaEliminado { get; set; }
        public DateTime FechaEliminacion { get; set; }

        /* Relaciones */
        public Comuna Comuna { get; set; }
        public Provincia Provincia { get; set; }
        public Region Region { get; set; }
        public Pais Pais { get; set; }

        public List<Sucursal> Sucursales { get; set; }
    }
}
