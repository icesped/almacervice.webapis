﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class Sucursal : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int NegocioId { get; set; }
        [Required]
        public bool EsMatriz { get; set; }
        public string Nombre { get; set; }
        public int Telefono { get; set; }
        [Required, MaxLength(100)]
        public string Direccion { get; set; }
        [Required]
        public int ComunaId { get; set; }
        [Required]
        public int ProvinciaId { get; set; }
        [Required]
        public int RegionId { get; set; }
        [Required]
        public int PaisId { get; set; }
        [Required]
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioModificador { get; set; }
        public bool EstaEliminado { get; set; }
        public DateTime FechaEliminacion { get; set; }
       

        /* Relaciones */  
        public Comuna Comuna { get; set; }
        public Provincia Provincia { get; set; }
        public Region Region { get; set; }
        public Pais Pais { get; set; }
        public Negocio Negocio { get; set; }
        public List<AccesoSucursal> AccesoSucursales { get; set; }
        public List<ProductoSucursal> ProductosSucursal { get; set; }
        public List<Cliente> Clientes { get; set; }
        public List<Proveedor> Proveedores { get; set; }
    }
}
