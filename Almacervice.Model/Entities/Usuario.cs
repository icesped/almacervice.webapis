﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class Usuario : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(8)]
        public int Rut { get; set; }
        [Required]
        public char Dv { get; set; }
        [Required, MaxLength(100)]
        public string Nombre { get; set; }
        [Required, MaxLength(50)]
        public string ApellidoPaterno { get; set; }
        [MaxLength(50)]
        public string ApellidoMaterno { get; set; }
        [Required, MaxLength(50)]
        public string Email { get; set; }
        [Required]
        public bool EmailConfirmado { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public Byte[] Salt { get; set; }
        public DateTime FechaUltimoLogin { get; set; }
        [Required]
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        [Required]
        public bool EstaSuspendido { get; set; }
        public DateTime FechaSuspension { get; set; }
        [Required]
        public bool EstaEliminado  { get; set; }
        public DateTime FechaEliminacion { get; set; }

        public List<AccesoSucursal> AccesoSucursales { get; set; }
    }
}
