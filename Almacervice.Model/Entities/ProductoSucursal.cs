﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class ProductoSucursal: IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int SucursalId { get; set; }
        [Required, MaxLength(13)]
        public long Codigo { get; set; }
        [Required, MaxLength(50)]
        public string Detalle { get; set; }
        [MaxLength(50)]
        public string Marca { get; set; }
        public double Peso { get; set; }
        public int UnidadMedidaId { get; set; }
        [Required]
        public int CategoriaId { get; set; }
        public int ProveedorId { get; set; }
        [Required]
        public int StockCritico { get; set; }
        public int NivelStock { get; set; }
        [Required]
        public double Costo { get; set; }
        [Required]
        public double Margen { get; set; }
        [Required]
        public int TipoMargenId { get; set; }
        [Required]
        public double Ganancia { get; set; }
        [Required]
        public double Precio { get; set; }
        public double Cantidad { get; set; }
        public double Total { get; set; }
        [Required]
        public DateTime FechaCreacion { get; set; }
        [Required]
        public bool EstaEliminado { get; set; }
        [Required]
        public DateTime FechaEliminacion { get; set; }
        [Required]
        public DateTime FechaModificacion { get; set; }
        [Required]
        public string UsuarioModificador { get; set; }

        /* Relaciones */
        public Sucursal Sucursal { get; set; }
        public Producto Producto { get; set; }
        public UnidadMedida UnidadMedida { get; set; }
        public Categoria Categoria { get; set; }
        public TipoMargen TipoMargen { get; set; }
        public Proveedor Proveedor { get; set; }

    }
}
