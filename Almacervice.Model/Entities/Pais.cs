﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class Pais : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(100)]
        public string Descripcion { get; set; }
        [Required, MaxLength(10)]
        public string Codigo { get; set; }

        /* Relaciones */
        public List<Negocio> Negocios { get; set; }
        public List<Sucursal> Sucursales { get; set; }
        public List<Region> Regiones { get; set; }
        public List<Proveedor> Proveedores { get; set; }
    }
}
