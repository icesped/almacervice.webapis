﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class PerfilUsuario : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string Perfil { get; set; }
        [Required, MaxLength(100)]
        public string Descripcion { get; set; }

        /* Relaciones */
        public List<AccesoSucursal> AccesoSucursales { get; set; }
    }
}
