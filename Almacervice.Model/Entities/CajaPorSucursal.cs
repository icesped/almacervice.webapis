﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class CajaPorSucursal: IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int SucursalId { get; set; }
        [Required]
        public int UsuarioId { get; set; }
        [Required]
        public bool EstaAbierta { get; set; }
        [Required]
        public DateTime FechaApertura { get; set; }
        [Required]
        public DateTime FechaCierre { get; set; }
        [Required]
        public int MontoApertura { get; set; }

    }
}
