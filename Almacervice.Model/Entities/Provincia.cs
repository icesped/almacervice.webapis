﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class Provincia : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(100)]
        public string Descripcion { get; set; }
        public int RegionId { get; set; }

        /* Relaciones */
        public Region Region { get; set; }
        public List<Negocio> Negocios { get; set; }
        public List<Sucursal> Sucursales { get; set; }
        public List<Comuna> Comunas { get; set; }
        public List<Proveedor> Proveedores { get; set; }
    }
}
