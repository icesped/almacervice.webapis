﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class AccesoSucursal : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        /* Foreign Keys */
        [Required]
        public int UsuarioId { get; set; }
        [Required]
        public int PerfilId { get; set; }
        [Required]
        public int SucursalId { get; set; }
        [Required]
        public bool EstaEliminado { get; set; }
        public DateTime FechaEliminacion { get; set; }

        /* Relaciones */
        public Sucursal Sucursal { get; set; }
        public Usuario  Usuario { get; set; }
        public PerfilUsuario Perfil { get; set; }


    }
}
