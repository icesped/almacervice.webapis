﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Model.Entities
{
    public class Proveedor : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int SucursalId { get; set; }
        [MaxLength(8)]
        public int Rut { get; set; }
        public char Dv { get; set; }
        [Required, MaxLength(100)]
        public string Nombre { get; set; }
        [MaxLength(50)]
        public string NombreContacto { get; set; }
        [MaxLength(50)]
        public string CargoContacto { get; set; }
        public int TelefonoContacto { get; set; }
        public int CelularContacto { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string SitioWeb { get; set; }
        [MaxLength(100)]
        public string Direccion { get; set; }
        public int PaisId { get; set; }
        public int RegionId { get; set; }
        public int ProvinciaId { get; set; }
        public int ComunaId { get; set; }
        [Required]
        public DateTime FechaCreacion { get; set; }
        [Required]
        public DateTime FechaModificacion { get; set; }
        [Required]
        public string UsuarioModificador { get; set; }
        [Required]
        public bool EstaEliminado { get; set; }
        public DateTime FechaEliminacion { get; set; }

        /* Relaciones */
        public Sucursal Sucursal { get; set; }
        public Comuna Comuna { get; set; }
        public Provincia Provincia { get; set; }
        public Region Region { get; set; }
        public Pais Pais { get; set; }
        public List<ProductoSucursal> ProductosSucursal { get; set; }
    }
}
