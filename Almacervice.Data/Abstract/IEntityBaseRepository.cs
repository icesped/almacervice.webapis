﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Almacervice.Model;
using System.Linq.Expressions;

namespace Almacervice.Data.Abstract
{
    public interface IEntityBaseRepository<T> where T : class, IEntityBase, new()
    {
        int Count();
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void DeleteWhere(Expression<Func<T, bool>> predicate);
        void Commit();
    }
}
