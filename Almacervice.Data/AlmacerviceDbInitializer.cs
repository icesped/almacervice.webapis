﻿using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data
{
    public class AlmacerviceDbInitializer
    {
        private static AlmacerviceContext _context;
        public static void Initialize(IServiceProvider serviceProvider)
        {
            _context = (AlmacerviceContext)serviceProvider.GetService(typeof(AlmacerviceContext));

            InitializeAlmacervice();
        }

        private static void InitializeAlmacervice()
        {
            AddCountries();
            AddRegions();
            AddProvinces();
            AddCommunes();
            AddUnits();
            AddCategories();
            AddMargins();
        }

        /// <summary>
        /// Inicializa la BD con Paises
        /// </summary>
        private static void AddCountries()
        {
            if (_context.Paises.Any())
                return;

            _context.AddRange(new Pais { Descripcion = "Chile", Codigo = "CL" });
            _context.SaveChanges();
        }

        /// <summary>
        /// Inicializa la BD con regiones
        /// </summary>
        private static void AddRegions()
        {
            if (_context.Regiones.Any())
                return;

            _context.AddRange(
                new Region { Descripcion = "Tarapacá", PaisId = 1 },
                new Region { Descripcion = "Antofagasta", PaisId = 1 },
                new Region { Descripcion = "Atacama", PaisId = 1 },
                new Region { Descripcion = "Coquimbo", PaisId = 1 },
                new Region { Descripcion = "Valparaíso", PaisId = 1 },
                new Region { Descripcion = "Región del Libertador Gral. Bernardo O’Higgins", PaisId = 1 },
                new Region { Descripcion = "Región del Maule", PaisId = 1 },
                new Region { Descripcion = "Región del Biobío", PaisId = 1 },
                new Region { Descripcion = "Región de la Araucanía", PaisId = 1 },
                new Region { Descripcion = "Región de Los Lagos", PaisId = 1 },
                new Region { Descripcion = "Región Aisén del Gral. Carlos Ibáñez del Campo", PaisId = 1 },
                new Region { Descripcion = "Región de Magallanes y de la Antártica Chilena", PaisId = 1 },
                new Region { Descripcion = "Región Metropolitana de Santiago", PaisId = 1 },
                new Region { Descripcion = "Región de Los Ríos", PaisId = 1 },
                new Region { Descripcion = "Arica y Parinacota", PaisId = 1 }
                );
            _context.SaveChanges();
        }

        /// <summary>
        /// Inicializa la BD con provincias
        /// </summary>
        private static void AddProvinces()
        {
            if (_context.Provincias.Any())
                return;
            #region Provincias
            _context.AddRange(
                new Provincia { Descripcion = "Iquique", RegionId = 1 },
                new Provincia { Descripcion = "Tamarugal", RegionId = 1 },
                new Provincia { Descripcion = "Antofagasta", RegionId = 2 },
                new Provincia { Descripcion = "El Loa", RegionId = 2 },
                new Provincia { Descripcion = "Tocopilla", RegionId = 2 },
                new Provincia { Descripcion = "Copiapó", RegionId = 3 },
                new Provincia { Descripcion = "Chañaral", RegionId = 3 },
                new Provincia { Descripcion = "Huasco", RegionId = 3 },
                new Provincia { Descripcion = "Elqui", RegionId = 4 },
                new Provincia { Descripcion = "Choapa", RegionId = 4 },
                new Provincia { Descripcion = "Limarí", RegionId = 4 },
                new Provincia { Descripcion = "Valparaíso", RegionId = 5 },
                new Provincia { Descripcion = "Isla de Pascua", RegionId = 5 },
                new Provincia { Descripcion = "Los Andes", RegionId = 5 },
                new Provincia { Descripcion = "Petorca", RegionId = 5 },
                new Provincia { Descripcion = "Quillota", RegionId = 5 },
                new Provincia { Descripcion = "San Antonio", RegionId = 5 },
                new Provincia { Descripcion = "San Felipe de Aconcagua", RegionId = 5 },
                new Provincia { Descripcion = "Marga Marga", RegionId = 5 },
                new Provincia { Descripcion = "Cachapoal", RegionId = 6 },
                new Provincia { Descripcion = "Cardenal Caro", RegionId = 6 },
                new Provincia { Descripcion = "Colchagua", RegionId = 6 },
                new Provincia { Descripcion = "Talca", RegionId = 7 },
                new Provincia { Descripcion = "Cauquenes", RegionId = 7 },
                new Provincia { Descripcion = "Curicó", RegionId = 7 },
                new Provincia { Descripcion = "Linares", RegionId = 7 },
                new Provincia { Descripcion = "Concepción", RegionId = 8 },
                new Provincia { Descripcion = "Arauco", RegionId = 8 },
                new Provincia { Descripcion = "Biobío", RegionId = 8 },
                new Provincia { Descripcion = "Ñuble", RegionId = 8 },
                new Provincia { Descripcion = "Cautín", RegionId = 9 },
                new Provincia { Descripcion = "Malleco", RegionId = 9 },
                new Provincia { Descripcion = "Llanquihue", RegionId = 10 },
                new Provincia { Descripcion = "Chiloé", RegionId = 10 },
                new Provincia { Descripcion = "Osorno", RegionId = 10 },
                new Provincia { Descripcion = "Palena", RegionId = 10 },
                new Provincia { Descripcion = "Coihaique", RegionId = 11 },
                new Provincia { Descripcion = "Aisén", RegionId = 11 },
                new Provincia { Descripcion = "Capitán Prat", RegionId = 11 },
                new Provincia { Descripcion = "General Carrera", RegionId = 11 },
                new Provincia { Descripcion = "Magallanes", RegionId = 12 },
                new Provincia { Descripcion = "Antártica Chilena", RegionId = 12 },
                new Provincia { Descripcion = "Tierra del Fuego", RegionId = 12 },
                new Provincia { Descripcion = "Última Esperanza", RegionId = 12 },
                new Provincia { Descripcion = "Santiago", RegionId = 13 },
                new Provincia { Descripcion = "Cordillera", RegionId = 13 },
                new Provincia { Descripcion = "Chacabuco", RegionId = 13 },
                new Provincia { Descripcion = "Maipo", RegionId = 13 },
                new Provincia { Descripcion = "Melipilla", RegionId = 13 },
                new Provincia { Descripcion = "Talagante", RegionId = 13 },
                new Provincia { Descripcion = "Valdivia", RegionId = 14 },
                new Provincia { Descripcion = "Ranco", RegionId = 14 },
                new Provincia { Descripcion = "Arica", RegionId = 15 },
                new Provincia { Descripcion = "Parinacota", RegionId = 15 }
                );
#endregion
            _context.SaveChanges();
        }

        /// <summary>
        /// Inicializa la BD con comunas
        /// </summary>
        private static void AddCommunes()
        {
            if (_context.Comunas.Any())
                return;
            #region Comunas
            _context.AddRange(
                new Comuna { Descripcion = "Iquique", ProvinciaId = 1 },
                new Comuna { Descripcion = "Alto Hospicio", ProvinciaId = 1 },
                new Comuna { Descripcion = "Pozo Almonte", ProvinciaId = 2 },
                new Comuna { Descripcion = "Camiña", ProvinciaId = 2 },
                new Comuna { Descripcion = "Colchane", ProvinciaId = 2 },
                new Comuna { Descripcion = "Huara", ProvinciaId = 2 },
                new Comuna { Descripcion = "Pica", ProvinciaId = 2 },
                new Comuna { Descripcion = "Antofagasta", ProvinciaId = 3 },
                new Comuna { Descripcion = "Mejillones", ProvinciaId = 3 },
                new Comuna { Descripcion = "Sierra Gorda", ProvinciaId = 3 },
                new Comuna { Descripcion = "Taltal", ProvinciaId = 3 },
                new Comuna { Descripcion = "Calama", ProvinciaId = 4 },
                new Comuna { Descripcion = "Ollagüe", ProvinciaId = 4 },
                new Comuna { Descripcion = "San Pedro de Atacama", ProvinciaId = 4 },
                new Comuna { Descripcion = "Tocopilla", ProvinciaId = 5 },
                new Comuna { Descripcion = "María Elena", ProvinciaId = 5 },
                new Comuna { Descripcion = "Copiapó", ProvinciaId = 6 },
                new Comuna { Descripcion = "Caldera", ProvinciaId = 6 },
                new Comuna { Descripcion = "Tierra Amarilla", ProvinciaId = 6 },
                new Comuna { Descripcion = "Chañaral", ProvinciaId = 7 },
                new Comuna { Descripcion = "Diego de Almagro", ProvinciaId = 7 },
                new Comuna { Descripcion = "Vallenar", ProvinciaId = 8 },
                new Comuna { Descripcion = "Alto del Carmen", ProvinciaId = 8 },
                new Comuna { Descripcion = "Freirina", ProvinciaId = 8 },
                new Comuna { Descripcion = "Huasco", ProvinciaId = 8 },
                new Comuna { Descripcion = "La Serena", ProvinciaId = 9 },
                new Comuna { Descripcion = "Coquimbo", ProvinciaId = 9 },
                new Comuna { Descripcion = "Andacollo", ProvinciaId = 9 },
                new Comuna { Descripcion = "La Higuera", ProvinciaId = 9 },
                new Comuna { Descripcion = "Paihuano", ProvinciaId = 9 },
                new Comuna { Descripcion = "Vicuña", ProvinciaId = 9 },
                new Comuna { Descripcion = "Illapel", ProvinciaId = 10 },
                new Comuna { Descripcion = "Canela", ProvinciaId = 10 },
                new Comuna { Descripcion = "Los Vilos", ProvinciaId = 10 },
                new Comuna { Descripcion = "Salamanca", ProvinciaId = 10 },
                new Comuna { Descripcion = "Ovalle", ProvinciaId = 11 },
                new Comuna { Descripcion = "Combarbalá", ProvinciaId = 11 },
                new Comuna { Descripcion = "Monte Patria", ProvinciaId = 11 },
                new Comuna { Descripcion = "Punitaqui", ProvinciaId = 11 },
                new Comuna { Descripcion = "Río Hurtado", ProvinciaId = 11 },
                new Comuna { Descripcion = "Valparaíso", ProvinciaId = 12 },
                new Comuna { Descripcion = "Casablanca", ProvinciaId = 12 },
                new Comuna { Descripcion = "Concón", ProvinciaId = 12 },
                new Comuna { Descripcion = "Juan Fernández", ProvinciaId = 12 },
                new Comuna { Descripcion = "Puchuncaví", ProvinciaId = 12 },
                new Comuna { Descripcion = "Quintero", ProvinciaId = 12 },
                new Comuna { Descripcion = "Viña del Mar", ProvinciaId = 12 },
                new Comuna { Descripcion = "Isla de Pascua", ProvinciaId = 13 },
                new Comuna { Descripcion = "Los Andes", ProvinciaId = 14 },
                new Comuna { Descripcion = "Calle Larga", ProvinciaId = 14 },
                new Comuna { Descripcion = "Rinconada", ProvinciaId = 14 },
                new Comuna { Descripcion = "San Esteban", ProvinciaId = 14 },
                new Comuna { Descripcion = "La Ligua", ProvinciaId = 15 },
                new Comuna { Descripcion = "Cabildo", ProvinciaId = 15 },
                new Comuna { Descripcion = "Papudo", ProvinciaId = 15 },
                new Comuna { Descripcion = "Petorca", ProvinciaId = 15 },
                new Comuna { Descripcion = "Zapallar", ProvinciaId = 15 },
                new Comuna { Descripcion = "Quillota", ProvinciaId = 16 },
                new Comuna { Descripcion = "La Calera", ProvinciaId = 16 },
                new Comuna { Descripcion = "Hijuelas", ProvinciaId = 16 },
                new Comuna { Descripcion = "La Cruz", ProvinciaId = 16 },
                new Comuna { Descripcion = "Nogales", ProvinciaId = 16 },
                new Comuna { Descripcion = "San Antonio", ProvinciaId = 17 },
                new Comuna { Descripcion = "Algarrobo", ProvinciaId = 17 },
                new Comuna { Descripcion = "Cartagena", ProvinciaId = 17 },
                new Comuna { Descripcion = "El Quisco", ProvinciaId = 17 },
                new Comuna { Descripcion = "El Tabo", ProvinciaId = 17 },
                new Comuna { Descripcion = "Santo Domingo", ProvinciaId = 17 },
                new Comuna { Descripcion = "San Felipe", ProvinciaId = 18 },
                new Comuna { Descripcion = "Catemu", ProvinciaId = 18 },
                new Comuna { Descripcion = "Llay Llay", ProvinciaId = 18 },
                new Comuna { Descripcion = "Panquehue", ProvinciaId = 18 },
                new Comuna { Descripcion = "Putaendo", ProvinciaId = 18 },
                new Comuna { Descripcion = "Santa María", ProvinciaId = 18 },
                new Comuna { Descripcion = "Quilpué", ProvinciaId = 19 },
                new Comuna { Descripcion = "Limache", ProvinciaId = 19 },
                new Comuna { Descripcion = "Olmué", ProvinciaId = 19 },
                new Comuna { Descripcion = "Villa Alemana", ProvinciaId = 19 },
                new Comuna { Descripcion = "Rancagua", ProvinciaId = 20 },
                new Comuna { Descripcion = "Codegua", ProvinciaId = 20 },
                new Comuna { Descripcion = "Coinco", ProvinciaId = 20 },
                new Comuna { Descripcion = "Coltauco", ProvinciaId = 20 },
                new Comuna { Descripcion = "Doñihue", ProvinciaId = 20 },
                new Comuna { Descripcion = "Graneros", ProvinciaId = 20 },
                new Comuna { Descripcion = "Las Cabras", ProvinciaId = 20 },
                new Comuna { Descripcion = "Machalí", ProvinciaId = 20 },
                new Comuna { Descripcion = "Malloa", ProvinciaId = 20 },
                new Comuna { Descripcion = "Mostazal", ProvinciaId = 20 },
                new Comuna { Descripcion = "Olivar", ProvinciaId = 20 },
                new Comuna { Descripcion = "Peumo", ProvinciaId = 20 },
                new Comuna { Descripcion = "Pichidegua", ProvinciaId = 20 },
                new Comuna { Descripcion = "Quinta de Tilcoco", ProvinciaId = 20 },
                new Comuna { Descripcion = "Rengo", ProvinciaId = 20 },
                new Comuna { Descripcion = "Requínoa", ProvinciaId = 20 },
                new Comuna { Descripcion = "San Vicente", ProvinciaId = 20 },
                new Comuna { Descripcion = "Pichilemu", ProvinciaId = 21 },
                new Comuna { Descripcion = "La Estrella", ProvinciaId = 21 },
                new Comuna { Descripcion = "Litueche", ProvinciaId = 21 },
                new Comuna { Descripcion = "Marchihue", ProvinciaId = 21 },
                new Comuna { Descripcion = "Navidad", ProvinciaId = 21 },
                new Comuna { Descripcion = "Paredones", ProvinciaId = 21 },
                new Comuna { Descripcion = "San Fernando", ProvinciaId = 22 },
                new Comuna { Descripcion = "Chépica", ProvinciaId = 22 },
                new Comuna { Descripcion = "Chimbarongo", ProvinciaId = 22 },
                new Comuna { Descripcion = "Lolol", ProvinciaId = 22 },
                new Comuna { Descripcion = "Nancagua", ProvinciaId = 22 },
                new Comuna { Descripcion = "Palmilla", ProvinciaId = 22 },
                new Comuna { Descripcion = "Peralillo", ProvinciaId = 22 },
                new Comuna { Descripcion = "Placilla", ProvinciaId = 22 },
                new Comuna { Descripcion = "Pumanque", ProvinciaId = 22 },
                new Comuna { Descripcion = "Santa Cruz", ProvinciaId = 22 },
                new Comuna { Descripcion = "Talca", ProvinciaId = 23 },
                new Comuna { Descripcion = "Constitución", ProvinciaId = 23 },
                new Comuna { Descripcion = "Curepto", ProvinciaId = 23 },
                new Comuna { Descripcion = "Empedrado", ProvinciaId = 23 },
                new Comuna { Descripcion = "Maule", ProvinciaId = 23 },
                new Comuna { Descripcion = "Pelarco", ProvinciaId = 23 },
                new Comuna { Descripcion = "Pencahue", ProvinciaId = 23 },
                new Comuna { Descripcion = "Río Claro", ProvinciaId = 23 },
                new Comuna { Descripcion = "San Clemente", ProvinciaId = 23 },
                new Comuna { Descripcion = "San Rafael", ProvinciaId = 23 },
                new Comuna { Descripcion = "Cauquenes", ProvinciaId = 24 },
                new Comuna { Descripcion = "Chanco", ProvinciaId = 24 },
                new Comuna { Descripcion = "Pelluhue", ProvinciaId = 24 },
                new Comuna { Descripcion = "Curicó", ProvinciaId = 25 },
                new Comuna { Descripcion = "Hualañé", ProvinciaId = 25 },
                new Comuna { Descripcion = "Licantén", ProvinciaId = 25 },
                new Comuna { Descripcion = "Molina", ProvinciaId = 25 },
                new Comuna { Descripcion = "Rauco", ProvinciaId = 25 },
                new Comuna { Descripcion = "Romeral", ProvinciaId = 25 },
                new Comuna { Descripcion = "Sagrada Familia", ProvinciaId = 25 },
                new Comuna { Descripcion = "Teno", ProvinciaId = 25 },
                new Comuna { Descripcion = "Vichuquén", ProvinciaId = 25 },
                new Comuna { Descripcion = "Linares", ProvinciaId = 26 },
                new Comuna { Descripcion = "Colbún", ProvinciaId = 26 },
                new Comuna { Descripcion = "Longaví", ProvinciaId = 26 },
                new Comuna { Descripcion = "Parral", ProvinciaId = 26 },
                new Comuna { Descripcion = "Retiro", ProvinciaId = 26 },
                new Comuna { Descripcion = "San Javier", ProvinciaId = 26 },
                new Comuna { Descripcion = "Villa Alegre", ProvinciaId = 26 },
                new Comuna { Descripcion = "Yerbas Buenas", ProvinciaId = 26 },
                new Comuna { Descripcion = "Concepción", ProvinciaId = 27 },
                new Comuna { Descripcion = "Coronel", ProvinciaId = 27 },
                new Comuna { Descripcion = "Chiguayante", ProvinciaId = 27 },
                new Comuna { Descripcion = "Florida", ProvinciaId = 27 },
                new Comuna { Descripcion = "Hualqui", ProvinciaId = 27 },
                new Comuna { Descripcion = "Lota", ProvinciaId = 27 },
                new Comuna { Descripcion = "Penco", ProvinciaId = 27 },
                new Comuna { Descripcion = "San Pedro de la Paz", ProvinciaId = 27 },
                new Comuna { Descripcion = "Santa Juana", ProvinciaId = 27 },
                new Comuna { Descripcion = "Talcahuano", ProvinciaId = 27 },
                new Comuna { Descripcion = "Tomé", ProvinciaId = 27 },
                new Comuna { Descripcion = "Hualpén", ProvinciaId = 27 },
                new Comuna { Descripcion = "Lebu", ProvinciaId = 28 },
                new Comuna { Descripcion = "Arauco", ProvinciaId = 28 },
                new Comuna { Descripcion = "Cañete", ProvinciaId = 28 },
                new Comuna { Descripcion = "Contulmo", ProvinciaId = 28 },
                new Comuna { Descripcion = "Curanilahue", ProvinciaId = 28 },
                new Comuna { Descripcion = "Los Álamos", ProvinciaId = 28 },
                new Comuna { Descripcion = "Tirúa", ProvinciaId = 28 },
                new Comuna { Descripcion = "Los Ángeles", ProvinciaId = 29 },
                new Comuna { Descripcion = "Antuco", ProvinciaId = 29 },
                new Comuna { Descripcion = "Cabrero", ProvinciaId = 29 },
                new Comuna { Descripcion = "Laja", ProvinciaId = 29 },
                new Comuna { Descripcion = "Mulchén", ProvinciaId = 29 },
                new Comuna { Descripcion = "Nacimiento", ProvinciaId = 29 },
                new Comuna { Descripcion = "Negrete", ProvinciaId = 29 },
                new Comuna { Descripcion = "Quilaco", ProvinciaId = 29 },
                new Comuna { Descripcion = "Quilleco", ProvinciaId = 29 },
                new Comuna { Descripcion = "San Rosendo", ProvinciaId = 29 },
                new Comuna { Descripcion = "Santa Bárbara", ProvinciaId = 29 },
                new Comuna { Descripcion = "Tucapel", ProvinciaId = 29 },
                new Comuna { Descripcion = "Yumbel", ProvinciaId = 29 },
                new Comuna { Descripcion = "Alto Biobío", ProvinciaId = 29 },
                new Comuna { Descripcion = "Chillán", ProvinciaId = 30 },
                new Comuna { Descripcion = "Bulnes", ProvinciaId = 30 },
                new Comuna { Descripcion = "Cobquecura", ProvinciaId = 30 },
                new Comuna { Descripcion = "Coelemu", ProvinciaId = 30 },
                new Comuna { Descripcion = "Coihueco", ProvinciaId = 30 },
                new Comuna { Descripcion = "Chillán Viejo", ProvinciaId = 30 },
                new Comuna { Descripcion = "El Carmen", ProvinciaId = 30 },
                new Comuna { Descripcion = "Ninhue", ProvinciaId = 30 },
                new Comuna { Descripcion = "Ñiquén", ProvinciaId = 30 },
                new Comuna { Descripcion = "Pemuco", ProvinciaId = 30 },
                new Comuna { Descripcion = "Pinto", ProvinciaId = 30 },
                new Comuna { Descripcion = "Portezuelo", ProvinciaId = 30 },
                new Comuna { Descripcion = "Quillón", ProvinciaId = 30 },
                new Comuna { Descripcion = "Quirihue", ProvinciaId = 30 },
                new Comuna { Descripcion = "Ránquil", ProvinciaId = 30 },
                new Comuna { Descripcion = "San Carlos", ProvinciaId = 30 },
                new Comuna { Descripcion = "San Fabián", ProvinciaId = 30 },
                new Comuna { Descripcion = "San Ignacio", ProvinciaId = 30 },
                new Comuna { Descripcion = "San Nicolás", ProvinciaId = 30 },
                new Comuna { Descripcion = "Treguaco", ProvinciaId = 30 },
                new Comuna { Descripcion = "Yungay", ProvinciaId = 30 },
                new Comuna { Descripcion = "Temuco", ProvinciaId = 31 },
                new Comuna { Descripcion = "Carahue", ProvinciaId = 31 },
                new Comuna { Descripcion = "Cunco", ProvinciaId = 31 },
                new Comuna { Descripcion = "Curarrehue", ProvinciaId = 31 },
                new Comuna { Descripcion = "Freire", ProvinciaId = 31 },
                new Comuna { Descripcion = "Galvarino", ProvinciaId = 31 },
                new Comuna { Descripcion = "Gorbea", ProvinciaId = 31 },
                new Comuna { Descripcion = "Lautaro", ProvinciaId = 31 },
                new Comuna { Descripcion = "Loncoche", ProvinciaId = 31 },
                new Comuna { Descripcion = "Melipeuco", ProvinciaId = 31 },
                new Comuna { Descripcion = "Nueva Imperial", ProvinciaId = 31 },
                new Comuna { Descripcion = "Padre las Casas", ProvinciaId = 31 },
                new Comuna { Descripcion = "Perquenco", ProvinciaId = 31 },
                new Comuna { Descripcion = "Pitrufquén", ProvinciaId = 31 },
                new Comuna { Descripcion = "Pucón", ProvinciaId = 31 },
                new Comuna { Descripcion = "Saavedra", ProvinciaId = 31 },
                new Comuna { Descripcion = "Teodoro Schmidt", ProvinciaId = 31 },
                new Comuna { Descripcion = "Toltén", ProvinciaId = 31 },
                new Comuna { Descripcion = "Vilcún", ProvinciaId = 31 },
                new Comuna { Descripcion = "Villarrica", ProvinciaId = 31 },
                new Comuna { Descripcion = "Cholchol", ProvinciaId = 31 },
                new Comuna { Descripcion = "Angol", ProvinciaId = 32 },
                new Comuna { Descripcion = "Collipulli", ProvinciaId = 32 },
                new Comuna { Descripcion = "Curacautín", ProvinciaId = 32 },
                new Comuna { Descripcion = "Ercilla", ProvinciaId = 32 },
                new Comuna { Descripcion = "Lonquimay", ProvinciaId = 32 },
                new Comuna { Descripcion = "Los Sauces", ProvinciaId = 32 },
                new Comuna { Descripcion = "Lumaco", ProvinciaId = 32 },
                new Comuna { Descripcion = "Purén", ProvinciaId = 32 },
                new Comuna { Descripcion = "Renaico", ProvinciaId = 32 },
                new Comuna { Descripcion = "Traiguén", ProvinciaId = 32 },
                new Comuna { Descripcion = "Victoria", ProvinciaId = 32 },
                new Comuna { Descripcion = "Puerto Montt", ProvinciaId = 33 },
                new Comuna { Descripcion = "Calbuco", ProvinciaId = 33 },
                new Comuna { Descripcion = "Cochamó", ProvinciaId = 33 },
                new Comuna { Descripcion = "Fresia", ProvinciaId = 33 },
                new Comuna { Descripcion = "Frutillar", ProvinciaId = 33 },
                new Comuna { Descripcion = "Los Muermos", ProvinciaId = 33 },
                new Comuna { Descripcion = "Llanquihue", ProvinciaId = 33 },
                new Comuna { Descripcion = "Maullín", ProvinciaId = 33 },
                new Comuna { Descripcion = "Puerto Varas", ProvinciaId = 33 },
                new Comuna { Descripcion = "Castro", ProvinciaId = 34 },
                new Comuna { Descripcion = "Ancud", ProvinciaId = 34 },
                new Comuna { Descripcion = "Chonchi", ProvinciaId = 34 },
                new Comuna { Descripcion = "Curaco de Vélez", ProvinciaId = 34 },
                new Comuna { Descripcion = "Dalcahue", ProvinciaId = 34 },
                new Comuna { Descripcion = "Puqueldón", ProvinciaId = 34 },
                new Comuna { Descripcion = "Queilén", ProvinciaId = 34 },
                new Comuna { Descripcion = "Quellón", ProvinciaId = 34 },
                new Comuna { Descripcion = "Quemchi", ProvinciaId = 34 },
                new Comuna { Descripcion = "Quinchao", ProvinciaId = 34 },
                new Comuna { Descripcion = "Osorno", ProvinciaId = 35 },
                new Comuna { Descripcion = "Puerto Octay", ProvinciaId = 35 },
                new Comuna { Descripcion = "Purranque", ProvinciaId = 35 },
                new Comuna { Descripcion = "Puyehue", ProvinciaId = 35 },
                new Comuna { Descripcion = "Río Negro", ProvinciaId = 35 },
                new Comuna { Descripcion = "San Juan de la Costa", ProvinciaId = 35 },
                new Comuna { Descripcion = "San Pablo", ProvinciaId = 35 },
                new Comuna { Descripcion = "Chaitén", ProvinciaId = 36 },
                new Comuna { Descripcion = "Futaleufú", ProvinciaId = 36 },
                new Comuna { Descripcion = "Hualaihué", ProvinciaId = 36 },
                new Comuna { Descripcion = "Palena", ProvinciaId = 36 },
                new Comuna { Descripcion = "Coyhaique", ProvinciaId = 37 },
                new Comuna { Descripcion = "Lago Verde", ProvinciaId = 37 },
                new Comuna { Descripcion = "Aysén", ProvinciaId = 38 },
                new Comuna { Descripcion = "Cisnes", ProvinciaId = 38 },
                new Comuna { Descripcion = "Guaitecas", ProvinciaId = 38 },
                new Comuna { Descripcion = "Cochrane", ProvinciaId = 39 },
                new Comuna { Descripcion = "O'Higgins", ProvinciaId = 39 },
                new Comuna { Descripcion = "Tortel", ProvinciaId = 39 },
                new Comuna { Descripcion = "Chile Chico", ProvinciaId = 40 },
                new Comuna { Descripcion = "Río Ibáñez", ProvinciaId = 40 },
                new Comuna { Descripcion = "Punta Arenas", ProvinciaId = 41 },
                new Comuna { Descripcion = "Laguna Blanca", ProvinciaId = 41 },
                new Comuna { Descripcion = "Río Verde", ProvinciaId = 41 },
                new Comuna { Descripcion = "San Gregorio", ProvinciaId = 41 },
                new Comuna { Descripcion = "Cabo de Hornos", ProvinciaId = 42 },
                new Comuna { Descripcion = "Antártica", ProvinciaId = 42 },
                new Comuna { Descripcion = "Porvenir", ProvinciaId = 43 },
                new Comuna { Descripcion = "Primavera", ProvinciaId = 43 },
                new Comuna { Descripcion = "Timaukel", ProvinciaId = 43 },
                new Comuna { Descripcion = "Natales", ProvinciaId = 44 },
                new Comuna { Descripcion = "Torres del Paine", ProvinciaId = 44 },
                new Comuna { Descripcion = "Santiago", ProvinciaId = 45 },
                new Comuna { Descripcion = "Cerrillos", ProvinciaId = 45 },
                new Comuna { Descripcion = "Cerro Navia", ProvinciaId = 45 },
                new Comuna { Descripcion = "Conchalí", ProvinciaId = 45 },
                new Comuna { Descripcion = "El Bosque", ProvinciaId = 45 },
                new Comuna { Descripcion = "Estación Central", ProvinciaId = 45 },
                new Comuna { Descripcion = "Huechuraba", ProvinciaId = 45 },
                new Comuna { Descripcion = "Independencia", ProvinciaId = 45 },
                new Comuna { Descripcion = "La Cisterna", ProvinciaId = 45 },
                new Comuna { Descripcion = "La Florida", ProvinciaId = 45 },
                new Comuna { Descripcion = "La Granja", ProvinciaId = 45 },
                new Comuna { Descripcion = "La Pintana", ProvinciaId = 45 },
                new Comuna { Descripcion = "La Reina", ProvinciaId = 45 },
                new Comuna { Descripcion = "Las Condes", ProvinciaId = 45 },
                new Comuna { Descripcion = "Lo Barnechea", ProvinciaId = 45 },
                new Comuna { Descripcion = "Lo Espejo", ProvinciaId = 45 },
                new Comuna { Descripcion = "Lo Prado", ProvinciaId = 45 },
                new Comuna { Descripcion = "Macul", ProvinciaId = 45 },
                new Comuna { Descripcion = "Maipú", ProvinciaId = 45 },
                new Comuna { Descripcion = "Ñuñoa", ProvinciaId = 45 },
                new Comuna { Descripcion = "Pedro Aguirre Cerda", ProvinciaId = 45 },
                new Comuna { Descripcion = "Peñalolén", ProvinciaId = 45 },
                new Comuna { Descripcion = "Providencia", ProvinciaId = 45 },
                new Comuna { Descripcion = "Pudahuel", ProvinciaId = 45 },
                new Comuna { Descripcion = "Quilicura", ProvinciaId = 45 },
                new Comuna { Descripcion = "Quinta Normal", ProvinciaId = 45 },
                new Comuna { Descripcion = "Recoleta", ProvinciaId = 45 },
                new Comuna { Descripcion = "Renca", ProvinciaId = 45 },
                new Comuna { Descripcion = "San Joaquín", ProvinciaId = 45 },
                new Comuna { Descripcion = "San Miguel", ProvinciaId = 45 },
                new Comuna { Descripcion = "San Ramón", ProvinciaId = 45 },
                new Comuna { Descripcion = "Vitacura", ProvinciaId = 45 },
                new Comuna { Descripcion = "Puente Alto", ProvinciaId = 46 },
                new Comuna { Descripcion = "Pirque", ProvinciaId = 46 },
                new Comuna { Descripcion = "San José de Maipo", ProvinciaId = 46 },
                new Comuna { Descripcion = "Colina", ProvinciaId = 47 },
                new Comuna { Descripcion = "Lampa", ProvinciaId = 47 },
                new Comuna { Descripcion = "Tiltil", ProvinciaId = 47 },
                new Comuna { Descripcion = "San Bernardo", ProvinciaId = 48 },
                new Comuna { Descripcion = "Buin", ProvinciaId = 48 },
                new Comuna { Descripcion = "Calera de Tango", ProvinciaId = 48 },
                new Comuna { Descripcion = "Paine", ProvinciaId = 48 },
                new Comuna { Descripcion = "Melipilla", ProvinciaId = 49 },
                new Comuna { Descripcion = "Alhué", ProvinciaId = 49 },
                new Comuna { Descripcion = "Curacaví", ProvinciaId = 49 },
                new Comuna { Descripcion = "María Pinto", ProvinciaId = 49 },
                new Comuna { Descripcion = "San Pedro", ProvinciaId = 49 },
                new Comuna { Descripcion = "Talagante", ProvinciaId = 50 },
                new Comuna { Descripcion = "El Monte", ProvinciaId = 50 },
                new Comuna { Descripcion = "Isla de Maipo", ProvinciaId = 50 },
                new Comuna { Descripcion = "Padre Hurtado", ProvinciaId = 50 },
                new Comuna { Descripcion = "Peñaflor", ProvinciaId = 50 },
                new Comuna { Descripcion = "Valdivia", ProvinciaId = 51 },
                new Comuna { Descripcion = "Corral", ProvinciaId = 51 },
                new Comuna { Descripcion = "Lanco", ProvinciaId = 51 },
                new Comuna { Descripcion = "Los Lagos", ProvinciaId = 51 },
                new Comuna { Descripcion = "Máfil", ProvinciaId = 51 },
                new Comuna { Descripcion = "Mariquina", ProvinciaId = 51 },
                new Comuna { Descripcion = "Paillaco", ProvinciaId = 51 },
                new Comuna { Descripcion = "Panguipulli", ProvinciaId = 51 },
                new Comuna { Descripcion = "La Unión", ProvinciaId = 52 },
                new Comuna { Descripcion = "Futrono", ProvinciaId = 52 },
                new Comuna { Descripcion = "Lago Ranco", ProvinciaId = 52 },
                new Comuna { Descripcion = "Río Bueno", ProvinciaId = 52 },
                new Comuna { Descripcion = "Arica", ProvinciaId = 53 },
                new Comuna { Descripcion = "Camarones", ProvinciaId = 53 },
                new Comuna { Descripcion = "Putre", ProvinciaId = 54 },
                new Comuna { Descripcion = "General Lagos", ProvinciaId = 54 }
                );
            #endregion
            _context.SaveChanges();
        }

        /// <summary>
        /// Inicializa la BD con unidades de medida
        /// </summary>
        private static void AddUnits()
        {
            if (_context.UnidadesMedidas.Any())
                return;
            _context.AddRange(
               new UnidadMedida { Detalle = "Gramos", Codigo = "g" },
               new UnidadMedida { Detalle = "Kilos", Codigo = "kg" },
               new UnidadMedida { Detalle = "Litros", Codigo = "lt" },
               new UnidadMedida { Detalle = "Milímetros", Codigo = "ml" },
               new UnidadMedida { Detalle = "Unidad", Codigo = "un" },
               new UnidadMedida { Detalle = "Docena", Codigo = "doc" },
               new UnidadMedida { Detalle = "Bandeja", Codigo = "ban" }
               );
            _context.SaveChanges();
        }

        /// <summary>
        /// Inicializa la BD con categorias
        /// </summary>
        private static void AddCategories()
        {
            if (_context.Categorias.Any())
                return;
            _context.AddRange(
               new Categoria { Detalle = "Despensa" },
               new Categoria { Detalle = "Carnes, Embutidos Y Pescados" },
               new Categoria { Detalle = "Lácteos y Huevos" },
               new Categoria { Detalle = "Pan, Frutas y Verduras" },
               new Categoria { Detalle = "Congelados y Postres" },
               new Categoria { Detalle = "Desayuno y Dulces" },
               new Categoria { Detalle = "Jugos, Bebidas y Licores" },
               new Categoria { Detalle = "Snacks y Aperitivos" },
               new Categoria { Detalle = "Limpieza y Hogar" },
               new Categoria { Detalle = "Mascotas" },
               new Categoria { Detalle = "Cosmética, Perfumería y Cuidado Personal" },
               new Categoria { Detalle = "Electrónica, Computación y Telefonía" },
               new Categoria { Detalle = "Librería y Celebraciones" },
               new Categoria { Detalle = "Infantil" },
               new Categoria { Detalle = "Automóvil" },
               new Categoria { Detalle = "Deportes y Aire Libre" },
               new Categoria { Detalle = "Especiales" }
               );
            _context.SaveChanges();
        }

        /// <summary>
        /// Inicializa la BD con tipos de margen de ganancia
        /// </summary>
        private static void AddMargins()
        {
            if (_context.TiposMargen.Any())
                return;
            _context.AddRange(
               new TipoMargen { Detalle = "Porcentaje", Codigo = "%" },
               new TipoMargen { Detalle = "Moneda", Codigo = "$"  }
               );
            _context.SaveChanges();
        }
    }
}
