﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Almacervice.Model.Entities;

namespace Almacervice.Data
{
    public class AlmacerviceContext : DbContext
    {
        public DbSet<Pais> Paises { get; set; }
        public DbSet<Region> Regiones { get; set; }
        public DbSet<Provincia> Provincias { get; set; }
        public DbSet<Comuna> Comunas { get; set; }
        public DbSet<Negocio> Negocios { get; set; }
        public DbSet<Sucursal> Sucursales { get; set; }
        public DbSet<PerfilUsuario> Perfiles { get; set; }
        public DbSet<AccesoSucursal> AccesosSucursales { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<UnidadMedida> UnidadesMedidas { get; set; }
        public DbSet<TipoMargen> TiposMargen { get; set; }
        public DbSet<ProductoSucursal> ProductosPorSucursal { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Proveedor> Proveedores { get; set; }

        public AlmacerviceContext(DbContextOptions options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /* PAIS */
            modelBuilder.Entity<Pais>()
                .HasMany(p => p.Regiones)
                .WithOne(r => r.Pais)
                .HasForeignKey(r => r.PaisId);

            modelBuilder.Entity<Pais>()
                .HasMany(p => p.Negocios)
                .WithOne(n => n.Pais)
                .HasForeignKey(n => n.PaisId);

            modelBuilder.Entity<Pais>()
                .HasMany(p => p.Sucursales)
                .WithOne(s => s.Pais)
                .HasForeignKey(s => s.PaisId);

            modelBuilder.Entity<Pais>()
                .HasMany(p => p.Proveedores)
                .WithOne(pr => pr.Pais)
                .HasForeignKey(pr => pr.PaisId);
            /***************************************/

            /* REGION */
            modelBuilder.Entity<Region>()
                .HasMany(r => r.Provincias)
                .WithOne(p => p.Region)
                .HasForeignKey(p => p.RegionId);

            modelBuilder.Entity<Region>()
                .HasMany(r => r.Negocios)
                .WithOne(n => n.Region)
                .HasForeignKey(n => n.RegionFacturacionId);

            modelBuilder.Entity<Region>()
                .HasMany(r => r.Sucursales)
                .WithOne(s => s.Region)
                .HasForeignKey(s => s.RegionId);

            modelBuilder.Entity<Region>()
                .HasMany(r => r.Proveedores)
                .WithOne(p => p.Region)
                .HasForeignKey(p => p.RegionId);
            /***************************************/

            /* PROVINCIA */
            modelBuilder.Entity<Provincia>()
                .HasMany(p => p.Comunas)
                .WithOne(c => c.Provincia)
                .HasForeignKey(c => c.ProvinciaId);

            modelBuilder.Entity<Provincia>()
                .HasMany(p => p.Negocios)
                .WithOne(n => n.Provincia)
                .HasForeignKey(n => n.ProvinciaFacturacionId);

            modelBuilder.Entity<Provincia>()
                .HasMany(p => p.Sucursales)
                .WithOne(s => s.Provincia)
                .HasForeignKey(s => s.ProvinciaId);

            modelBuilder.Entity<Provincia>()
                .HasMany(p => p.Proveedores)
                .WithOne(pr => pr.Provincia)
                .HasForeignKey(pr => pr.ProvinciaId);
            /***************************************/

            /* COMUNA */
            modelBuilder.Entity<Comuna>()
                .HasMany(c => c.Negocios)
                .WithOne(n => n.Comuna)
                .HasForeignKey(n => n.ComunaFacturacionId);

            modelBuilder.Entity<Comuna>()
                .HasMany(c => c.Sucursales)
                .WithOne(s => s.Comuna)
                .HasForeignKey(s => s.ComunaId);

            modelBuilder.Entity<Comuna>()
                .HasMany(c => c.Proveedores)
                .WithOne(p => p.Comuna)
                .HasForeignKey(p => p.ComunaId);
            /***************************************/

            /* NEGOCIO */
            modelBuilder.Entity<Negocio>()
                .HasIndex(n => n.Rut);
            modelBuilder.Entity<Negocio>()
                .HasMany(n => n.Sucursales)
                .WithOne(s => s.Negocio)
                .HasForeignKey(s => s.NegocioId);
            /***************************************/

            /* SUCURSAL */
            modelBuilder.Entity<Sucursal>()
                .HasMany(s => s.AccesoSucursales)
                .WithOne(a => a.Sucursal)
                .HasForeignKey(a => a.SucursalId);

            modelBuilder.Entity<Sucursal>()
                .HasMany(s => s.ProductosSucursal)
                .WithOne(p => p.Sucursal)
                .HasForeignKey(p => p.SucursalId);

            modelBuilder.Entity<Sucursal>()
                .HasMany(s => s.Clientes)
                .WithOne(c => c.Sucursal)
                .HasForeignKey(c => c.SucursalId);

            modelBuilder.Entity<Sucursal>()
                .HasMany(s => s.Proveedores)
                .WithOne(p => p.Sucursal)
                .HasForeignKey(p => p.SucursalId);
            /***************************************/

            /* ACCESO SUCURSALES */
            modelBuilder.Entity<AccesoSucursal>()
                .ToTable("acceso_sucursal");
            modelBuilder.Entity<AccesoSucursal>()
                .HasIndex(a => new { a.SucursalId, a.UsuarioId, a.PerfilId });
            /***************************************/

            /*PERFIL USUARIO*/
            modelBuilder.Entity<PerfilUsuario>()
                .ToTable("perfil_usuario");
            modelBuilder.Entity<PerfilUsuario>()
                .HasMany(pu => pu.AccesoSucursales)
                .WithOne(a => a.Perfil)
                .HasForeignKey(a => a.PerfilId);
            /***************************************/

            /* USUARIO */
            modelBuilder.Entity<Usuario>()
                .HasIndex(u => u.Rut);
            modelBuilder.Entity<Usuario>()
                .HasMany(u => u.AccesoSucursales)
                .WithOne(a => a.Usuario)
                .HasForeignKey(a => a.UsuarioId);
            /***************************************/

            /* UNIDAD MEDIDA */
            modelBuilder.Entity<UnidadMedida>()
                 .ToTable("unidad_medida");
            modelBuilder.Entity<UnidadMedida>()
                .HasMany(um => um.Productos)
                .WithOne(p => p.UnidadMedida)
                .HasForeignKey(p => p.UnidadMedidaId);
            modelBuilder.Entity<UnidadMedida>()
                .HasMany(um => um.ProductosSucursal)
                .WithOne(ps => ps.UnidadMedida)
                .HasForeignKey(ps => ps.UnidadMedidaId);
            /***************************************/

            /* PRODUCTO */
            modelBuilder.Entity<Producto>();
            /***************************************/

            /* PRODUCTO POR SUCURSAL */
            modelBuilder.Entity<ProductoSucursal>()
                .ToTable("producto_por_sucursal")
                .HasIndex(ps => ps.Codigo);
            /***************************************/

            /* TIPOS DE MARGEN */
            modelBuilder.Entity<TipoMargen>()
                .HasMany(tm => tm.ProductosSucursal)
                .WithOne(ps => ps.TipoMargen)
                .HasForeignKey(ps => ps.TipoMargenId);
            /***************************************/

            /* CATEGORÍA */
            modelBuilder.Entity<Categoria>()
                .HasMany(c => c.ProductosSucursal)
                .WithOne(ps => ps.Categoria)
                .HasForeignKey(ps => ps.CategoriaId);
            /***************************************/

            /* CLIENTE */
            modelBuilder.Entity<Cliente>()
                .HasIndex(c => c.Rut);
            /***************************************/

            /* PROVEEDOR */
            modelBuilder.Entity<Proveedor>()
                .HasMany(p => p.ProductosSucursal)
                .WithOne(ps => ps.Proveedor)
                .HasForeignKey(ps => ps.ProveedorId);
            /***************************************/

        }
    }
}
