﻿using Almacervice.Data.Abstract;
using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class SucursalRepository : EntityBaseRepository<Sucursal>
    {
        public SucursalRepository(AlmacerviceContext context) : base(context) { }

        /// <summary>
        /// Obtiene todas las sucursales
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Sucursal>> GetAllSubsidiaries()
        {
            return Task.FromResult<IEnumerable<Sucursal>>(_context.Sucursales.ToList());
        }

        /// <summary>
        /// Obtiene una sucursal por medio de su id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Sucursal> GetSubsidiaryById(int id)
        {
            return Task.FromResult(_context.Sucursales.FirstOrDefault(sub => sub.Id == id));
        }

        /// <summary>
        /// Obtiene todas las sucursales asociadas a un negocio
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Sucursal>> GetAllSubsidiariesByStoreId(int storeId)
        {
            return Task.FromResult<IEnumerable<Sucursal>>(_context.Sucursales.
                Where(s => s.NegocioId == storeId && s.EstaEliminado == false).ToList());
        }
    }
}
