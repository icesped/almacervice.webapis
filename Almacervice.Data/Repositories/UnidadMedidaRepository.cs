﻿using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class UnidadMedidaRepository : EntityBaseRepository<UnidadMedida>
    {
        public UnidadMedidaRepository(AlmacerviceContext context) : base(context){ }

        /// <summary>
        /// Obtiene todas las Unidades de Medida
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<UnidadMedida>> GetAllUnits()
        {
            return Task.FromResult<IEnumerable<UnidadMedida>>(_context.UnidadesMedidas.ToList());
        }

        /// <summary>
        /// Obtiene una Unidad de Medida por su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<UnidadMedida> GetUnitById(int id)
        {
            return Task.FromResult(_context.UnidadesMedidas.FirstOrDefault(um => um.Id == id));
        }
    }
}
