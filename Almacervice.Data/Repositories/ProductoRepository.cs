﻿using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class ProductoRepository: EntityBaseRepository<Producto>
    {
        public ProductoRepository(AlmacerviceContext context) : base(context)
        {
        }

        public IEnumerable<Producto> GetAllProducts()
        {
            return _context.Productos.ToList();
        }

        public Producto GetProductById(int id)
        {
            return _context.Productos.FirstOrDefault(p => p.Id == id);
        }

        public Task<Producto> GetProductByBarcode(long barcode)
        {
            return Task.FromResult(_context.Productos.FirstOrDefault(p => p.Codigo == barcode));
        }
    }
}
