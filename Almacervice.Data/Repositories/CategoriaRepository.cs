﻿using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class CategoriaRepository : EntityBaseRepository<Categoria>
    {
        public CategoriaRepository(AlmacerviceContext context) : base(context){ }

        /// <summary>
        /// Obtiene todas las categorias
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Categoria>> GetAllCategories()
        {
            return Task.FromResult<IEnumerable<Categoria>>(_context.Categorias.ToList());
        }

        /// <summary>
        /// Obtiene una categoria por su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Categoria> GetCategoryById(int id)
        {
            return Task.FromResult(_context.Categorias.FirstOrDefault(c => c.Id == id));
        }
    }
}
