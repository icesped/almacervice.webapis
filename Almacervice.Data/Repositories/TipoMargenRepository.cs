﻿using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class TipoMargenRepository : EntityBaseRepository<TipoMargen>
    {
        public TipoMargenRepository(AlmacerviceContext context) : base(context){ }

        /// <summary>
        /// Obtiene todos los tipos de margen
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<TipoMargen>> GetAllMarginTypes()
        {
            return Task.FromResult<IEnumerable<TipoMargen>>(_context.TiposMargen.ToList());
        }

        /// <summary>
        /// Obtiene un tipo de Margen por si Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<TipoMargen> GetMarginTypeById(int id)
        {
            return Task.FromResult(_context.TiposMargen.FirstOrDefault(c => c.Id == id));
        }
    }
}
