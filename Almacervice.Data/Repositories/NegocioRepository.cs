﻿using Almacervice.Data.Abstract;
using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class NegocioRepository : EntityBaseRepository<Negocio>
    {
        public NegocioRepository(AlmacerviceContext context) : base(context){ }

        public IEnumerable<Negocio> GetAllStores()
        {
            return _context.Negocios.ToList();
        }

        public Negocio GetStoreById(int id)
        {
            return _context.Negocios.FirstOrDefault(s => s.Id == id);
        }

        public Negocio GetStoreByRut(int rut)
        {
            return _context.Negocios.FirstOrDefault(s => s.Rut == rut);
        }
    }
}
