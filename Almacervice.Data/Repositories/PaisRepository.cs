﻿using Almacervice.Data.Abstract;
using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class PaisRepository : EntityBaseRepository<Pais>
    {
        public PaisRepository(AlmacerviceContext context) : base(context)
        {
        }

        public IEnumerable<Pais> GetAllCountries()
        {
            return _context.Paises.ToList();
        }

        public Pais GetCountry(int id)
        {
            return _context.Paises.FirstOrDefault(p => p.Id == id);
        }
    }
}
