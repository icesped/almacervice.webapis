﻿using Almacervice.Data.Abstract;
using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class UsuarioRepository : EntityBaseRepository<Usuario>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public UsuarioRepository(AlmacerviceContext context) : base(context)
        {
        }

        /// <summary>
        /// Obtiene todos los usuarios
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Usuario>> GetAllUsers()
        {
            return Task.FromResult<IEnumerable<Usuario>>(_context.Usuarios.ToList().Where(u => u.EstaEliminado == false));
        }

        /// <summary>
        /// Obtiene un usuario mediante su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Usuario> GetUserById(int id)
        {
            return Task.FromResult(_context.Usuarios.FirstOrDefault(u => u.Id == id && u.EstaEliminado == false));
        }

        /// <summary>
        /// Obtiene un usuario mediante el Rut
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        public Usuario GetUserByRut(int rut)
        {
            return _context.Usuarios.FirstOrDefault(u => u.Rut == rut && u.EstaEliminado == false);
        }
    }
}
