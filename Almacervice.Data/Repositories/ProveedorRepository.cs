﻿using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class ProveedorRepository : EntityBaseRepository<Proveedor>
    {
        public ProveedorRepository(AlmacerviceContext context) : base(context){ }

        /// <summary>
        /// Obtiene todos los proveedores asociados a una sucursal
        /// </summary>
        /// <param name="subsidiaryId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Proveedor>> GetAllProvidersBySubsidiaryId(int subsidiaryId)
        {
            return Task.FromResult<IEnumerable<Proveedor>>(_context.Proveedores.
               Where(p => p.SucursalId == subsidiaryId && p.EstaEliminado == false).ToList());
        }

        /// <summary>
        /// Obtiene un proveedor de acuerdo a una sucursal y su id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="subsidiaryId"></param>
        /// <returns></returns>
        public Task<Proveedor> GetProviderBySubsidiaryId(int id, int subsidiaryId)
        {
            return Task.FromResult(_context.Proveedores.FirstOrDefault(p => p.SucursalId == subsidiaryId && p.Id == id && p.EstaEliminado == false));
        }

        /// <summary>
        /// Obtiene un proveedor por medio de su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Proveedor> GetProviderById(int id)
        {
            return Task.FromResult(_context.Proveedores.FirstOrDefault(p => p.Id == id && p.EstaEliminado == false));
        }
    }
}
