﻿using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class ProductoSucursalRepository: EntityBaseRepository<ProductoSucursal>
    {
        public ProductoSucursalRepository(AlmacerviceContext context) : base(context)
        {
        }

        /// <summary>
        /// Obtiene un producto por su Id que no este eliminado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<ProductoSucursal> GetProductById(int id)
        {
            return Task.FromResult(_context.ProductosPorSucursal.FirstOrDefault(p => p.Id == id && p.EstaEliminado == false));
        }

        /// <summary>
        /// Obtiene todos los productos de una sucursal que no esten eliminados
        /// </summary>
        /// <param name="subsidiaryId"></param>
        /// <returns></returns>
        public Task<IEnumerable<ProductoSucursal>> GetAllProductsBySubsidiaryId(int subsidiaryId)
        {
            return Task.FromResult<IEnumerable<ProductoSucursal>>(_context.ProductosPorSucursal.
                Where(p => p.SucursalId == subsidiaryId && p.EstaEliminado == false).ToList());
        }

        /// <summary>
        /// Obtiene un producto por su codigo y sucursal
        /// </summary>
        /// <param name="barcode"></param>
        /// <param name="subsidiaryId"></param>
        /// <returns></returns>
        public Task<ProductoSucursal> GetProductByBarcodeAndSubsidiary(long barcode, int subsidiaryId)
        {
            return Task.FromResult(_context.ProductosPorSucursal
                .FirstOrDefault(p => p.SucursalId == subsidiaryId && p.Codigo == barcode && p.EstaEliminado == false));
        }
    }
}
