﻿using Almacervice.Data.Abstract;
using Almacervice.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Almacervice.Data.Repositories
{
    public class AccesoSucursalRepository : EntityBaseRepository<AccesoSucursal>
    {
        public AccesoSucursalRepository(AlmacerviceContext context) : base(context) { }

        public IEnumerable<AccesoSucursal> GetAllAccessToSubsidiaries()
        {
            return _context.AccesosSucursales.ToList();
        }

        public AccesoSucursal GetAccessToSubsidiaryById(int id)
        {
            return _context.AccesosSucursales.FirstOrDefault(a => a.Id == id);
        }

        public IEnumerable<AccesoSucursal> GetAccessToSubsidiariesByUserId(int userId)
        {
            return from access in _context.AccesosSucursales
                   where access.UsuarioId == userId
                   select access;
        }
    }
}
